
<!DOCTYPE HTML>
<html>
	<head>
		<title>Sistema Recolector de Materiales Reciclables</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />

  <!-- iCheck -->
	</head>
	<body class="is-preload">

		<!-- Wrapper -->
			<div id="wrapper">

				<!-- Header -->
					<header id="header">
						<h1><a href="index.php">S.R.M.R.</a></h1>
						<nav class="links">
							<ul>
								<li><a href="index.php">Inicio</a></li>
								<li><a href="quines-somos.php">Quiénes Somos</a></li>
								<li><a href="contactenos.php">Contáctanos</a></li>
							</ul>
						</nav>
						<nav class="main">
							<ul>
								<li class="search">
									<a class="fa-search" href="#search">Buscar</a>
									<form id="search" method="get" action="#">
										<input type="text" name="query" placeholder="Buscar" />
									</form>
								</li>
								<li class="menu">
									<a class="fa-bars" href="#menu">Menú</a>
								</li>
							</ul>
						</nav>
					</header>

									<!-- Menu -->
					<section id="menu">

						<!-- Search -->
							<section>
								<form class="search" method="get" action="#">
									<input type="text" name="query" placeholder="Buscar" />
								</form>
							</section>

						<!-- Links -->
							<section>
								<ul class="links">
									<li>
										<a href="index.php">
											<h3>Inicio</h3>
											<p>Conoce nuestros eventos...</p>
										</a>
									</li>
									<li>
										<a href="quines-somos.php">
											<h3>Quiénes Somos</h3>
											<p>Conoce más sobre nuestro proyecto...</p>
										</a>
									</li>
									<li>
										<a href="contactenos.php">
											<h3>Contáctanos</h3>
											<p>Comparte tu experiencia con nosotros...</p>
										</a>
									</li>
								</ul>
							</section>

						<!-- Actions -->
							<section>
								<ul class="actions stacked">
									<li><a href="pages/form/login.html" class="button large fit">Iniciar Sesión</a></li>
								</ul>
							</section>

					</section>

<?php 
  session_start();
  $NombreSesion =  $_SESSION["nombre"]  ;
  $ApellidoSesion =  $_SESSION["apelido"] ;
  $PerfilSesion =  $_SESSION["id_perfil"] ;
  $idSesion =  $_SESSION["id_usuario"] ;
  include_once("../../php/libreria.php");

if(empty($_SESSION["id_usuario"])){
  header("refresh:0; url=../form/login.html");
}

  $objCrudSesion = new Crud();
  $objCrudSesion->tablas = "perfil_usuario";
  $objCrudSesion->expresion ="*";
  $objCrudSesion->condicion ="id_perfil = '$PerfilSesion ' ";
  $contar=$objCrudSesion->read();
  $arreglo=$objCrudSesion->filas;
  $nomPerfil=$arreglo[0]["perfil"];
  $objCrud            = new Crud();
  $objCrud->tablas        = "evento";
  $objCrud->expresion     = "*";
  $objUtilidades          = new Utilidades();
    

    if(isset($_REQUEST["btnBusqueda"])){
      $vrtexBusqueda  = htmlspecialchars($_REQUEST['txtBusqueda']);

          $objCrud->condicion = "nombre like '%$vrtexBusqueda%'";

      }

  $objCrud->ordenamiento = "id_evento ASC, nombre ASC";
  $vrleerEvento          = $objCrud->read();

    if($vrleerEvento==0){

      $_SESSION['message'] = 'no hay evento para consultar';
      $_SESSION['message_type'] = 'primary';
      header("refresh:1; url=newevent.php");
  }
  $arregloEvento = $objCrud->filas;
?>


<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Eventos</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="../../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<script type="text/javascript">
  function comfirmDelete(){
    var respuesta =confirm("Estas seguro que deseas eliminar el/los evento(s)");
    if(respuesta == true){
      return true;
    }
    else {
      return false;
    }
  }
</script>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  
      <?php 
if($PerfilSesion==1){
include ("../../include/navAdmin.php"); 
}else{
  include ("../../include/navOperario.php");
}
?>
    

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Lista de Eventos</h1>
          </div>
        </div>
        <?php if(isset($_SESSION['message'])) {?>

        <div class="alert alert-<?=$_SESSION['message_type'];?> alert-dismissible fade show" role="alert">    <?= $_SESSION['message']?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

      <?php $_SESSION['message'] = null; }?>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- /.col -->
        <div class="col-md-12">
          <div class="card card-primary card-outline">
            <form name="frmBusqueda" id="frmBusqueda" method="get" action='<?php echo $_SERVER["PHP_SELF"]; ?>'>
            <div class="card-header">
              <h3 class="card-title">Eventos</h3>

              
              <div class="card-tools">
                <div class="input-group input-group-sm">
                  
                    <input type="text" class="form-control" name="txtBusqueda" id="txtBusqueda" placeholder="Nombre">
                    <div class="input-group-append">
                      <div class="btn btn-primary">
                        <button type="btnBusqueda" name="btnBusqueda" id="btnBusqueda">
                          <i class="fas fa-search" ></i>
                        </button>
                        
                      </div>
                    </div>
                  </div>
              </div>
              

              <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            </form>
              <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
              <form action="deleteevent.php" method="post">
              <div class="mailbox-controls">
                <!-- Check all button -->
                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="far fa-square"></i>
                </button>
                <div class="btn-group">
                  <button type="submit" name="btnEliminar" title="eliminar" class="btn btn-default btn-sm" onclick="return comfirmDelete()"><i class="far fa-trash-alt"></i></button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fas fa-reply"></i></button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fas fa-share"></i></button>
                </div>
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-sync-alt"></i></button>
                <div class="float-right">
                  1-50/200
                  <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-left"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-right"></i></button>
                  </div>
                  <!-- /.btn-group -->
                </div>
                <!-- /.float-right -->
              </div>
              <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
                  <tbody>
                  <tr>
                    <td>
                    </td>
                    <td class="mailbox-name">Título</td>
                    <td class="mailbox-subject">Descripción</td>
                    <td class="mailbox-attachment">Lugar</td>
                    <td class="mailbox-date">Fecha Inicio</td>
                    <td class="mailbox-date">Fecha Fin</td>
                    <td class="mailbox-date">Hora Inicio</td>
                    <td class="mailbox-date">Hora Fin</td>
                    <td class="mailbox-subject">Editar</td>
                  </tr>
                  <?php 
                       $vrno = 1;
                    foreach($arregloEvento as $valor){


                        $vridEvento              =$valor["id_evento"];
                        $vridUsuario             =$valor["id_usuario"];
                        $vridTiporeciclaje       =$valor["id_tipo_reciclaje"];
                        $vrnombreEvento          =$valor["nombre"];
                        $vrlugarEvento         =$valor["lugar"];
                        $vrhoraiEvento         =$valor["h_inicio"];
                        $vrhorafEvento         =$valor["h_final"];
                        $vrfechaIEvento           =$valor["fecha_i"];
                        $vrfechaFEvento           =$valor["fecha_f"];
                        $vrobservacionesEvento   =$valor["observaciones"];
                        
                       
                         
                        echo "<tr>";
                        echo "<td>";
                        echo "<input type='checkbox' class='icheck-primary' name='idEve[]'  value=$vridEvento >";
                        echo "</td>";
                        echo "<td class='mailbox-name'>$vrnombreEvento</td>";
                        echo "<td class='mailbox-subject'>$vrobservacionesEvento</td>";
                        echo "<td class='mailbox-attachment'>$vrlugarEvento</td>";
                        echo "<td class='mailbox-date'>$vrfechaIEvento</td>";
                        echo "<td class='mailbox-date'>$vrfechaFEvento</td>";
                        echo "<td class='mailbox-date'>$vrhoraiEvento</td>";
                        echo "<td class='mailbox-date'>$vrhorafEvento</td>";
                        echo "<td class='mailbox-subject'><a title='editar' href='editevent.php?id_evento=$vridEvento'><i class ='far fa-edit'></i></a></td>";

                        echo "</tr>";
                        $vrno++;
                      
                       }
                     ?>          
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.card-body -->
            <div class="card-footer p-0">
              <div class="mailbox-controls">
                <!-- Check all button -->
                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="far fa-square"></i>
                </button>
                <div class="btn-group">
                  <button type="submit" name="btnEliminar" title="eliminar" class="btn btn-default btn-sm" onclick="return comfirmDelete()"><i class="far fa-trash-alt"></i></button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fas fa-reply"></i></button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fas fa-share"></i></button>
                </div>
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-sync-alt"></i></button>
                <div class="float-right">
                  1-50/200
                  <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-left"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-right"></i></button>
                  </div>
                  <!-- /.btn-group -->
                </div>
                <!-- /.float-right -->
              </div>
              </form>
            </div>
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- Page Script -->
<script>
  $(function () {
    //Enable check and uncheck all functionality
    $('.checkbox-toggle').click(function () {
      var clicks = $(this).data('clicks')
      if (clicks) {
        //Uncheck all checkboxes
        $('.mailbox-messages input[type=\'checkbox\']').prop('checked', false)
        $('.checkbox-toggle .far.fa-check-square').removeClass('fa-check-square').addClass('fa-square')
      } else {
        //Check all checkboxes
        $('.mailbox-messages input[type=\'checkbox\']').prop('checked', true)
        $('.checkbox-toggle .far.fa-square').removeClass('fa-square').addClass('fa-check-square')
      }
      $(this).data('clicks', !clicks)
    })

    //Handle starring for glyphicon and font awesome
    $('.mailbox-star').click(function (e) {
      e.preventDefault()
      //detect type
      var $this = $(this).find('a > i')
      var glyph = $this.hasClass('glyphicon')
      var fa    = $this.hasClass('fa')

      //Switch states
      if (glyph) {
        $this.toggleClass('glyphicon-star')
        $this.toggleClass('glyphicon-star-empty')
      }

      if (fa) {
        $this.toggleClass('fa-star')
        $this.toggleClass('fa-star-o')
      }
    })
  })
</script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>

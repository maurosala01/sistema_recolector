<?php 
session_start();
  $NombreSesion =  $_SESSION["nombre"]  ;
  $ApellidoSesion =  $_SESSION["apelido"] ;
  $PerfilSesion =  $_SESSION["id_perfil"] ;
  $idSesion =  $_SESSION["id_usuario"] ;
  include_once("../../php/libreria.php");
  if(empty($_SESSION["id_usuario"])){
  header("refresh:0; url=../form/login.html");
}
  $objCrudSesion = new Crud();
  $objCrudSesion->tablas = "perfil_usuario";
  $objCrudSesion->expresion ="*";
  $objCrudSesion->condicion ="id_perfil = '$PerfilSesion ' ";
  $contar=$objCrudSesion->read();
  $arreglo=$objCrudSesion->filas;
  $nomPerfil=$arreglo[0]["perfil"];
  $objConexion   = new Conexion(); 
    $idconexion    = $objConexion->conectar();
    $objUtilidades = new Utilidades();
    $vridTipoReciclaje = $objUtilidades->consecutivo("tipo_reciclaje","id_tipo_reciclaje",$idconexion);
    $vrTRNombre= "";
    $vrTRCaracteristicas= "";
    $vrTRPuntos="";
    if(isset($_REQUEST["btnAuxiliar"])){
      $vrTRNombre=        htmlspecialchars($_REQUEST["txtTRNombre"]);
      $vrTRCaracteristicas=   htmlspecialchars($_REQUEST["txtTRCaracteristicas"]);
      $vrTRPuntos=        htmlspecialchars($_REQUEST["numTRPuntos"]);

      $objCrud          = new Crud();
      $objCrud->tablas  = "tipo_reciclaje";
      $objCrud->campos  = "id_tipo_reciclaje, nombre, caracteristicas, puntos_botella";
      $objCrud->valores ="'$vridTipoReciclaje','$vrTRNombre','$vrTRCaracteristicas','$vrTRPuntos'";
      $objCrud->create($idconexion);
      $_SESSION['message'] = 'Tipo de reciclaje Registrado!!!';
      $_SESSION['message_type'] = 'success';
    }
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Nuevo tipo de reciclaje</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
    <!-- Right navbar links -->
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  
<?php 
if($PerfilSesion==1){
include ("../../include/navAdmin.php"); 
}else{
  include ("../../include/navOperario.php");
}
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>TIPO RECICLAJE</h1>
          </div>
        </div>
        <?php if(isset($_SESSION['message'])) {?>

        <div class="alert alert-<?=$_SESSION['message_type'];?> alert-dismissible fade show" role="alert">    <?= $_SESSION['message']?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

      <?php $_SESSION['message'] = null; }?>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="../../dist/img/logo.png"
                       alt="User profile picture">
                </div>
                <h3 class="profile-username text-center"> <?php echo $NombreSesion." ".$ApellidoSesion;  ?></h3>
              </div>
              <!-- /.card-body -->
            </div>

            <!-- /.card -->
          </div>
          <div class="col-md-8">
            <form name="frmingTipoReciclaje" id="frmingTipoReciclaje" method="post"  action='<?php echo $_SERVER["PHP_SELF"]; ?>'>
             <div class="card card-primary card-outline">
              <!-- /.card-header -->
              <div class="card-body">
                <div class="form-group m-0">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                       	 <label>Nombre:</label>
                       	 <input class="form-control" type="text" name="txtTRNombre" id="txtTRNombre">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                       	 <label>Características:</label>
                       	 <input class="form-control" type="text" name="txtTRCaracteristicas" id="txtTRCaracteristicas">
                        </div>
                    </div>
                	</div>
                </div>
                <div class="form-group m-0">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                       	 <label>Puntos por botella:</label>
							<input class="form-control" type="number" name="numTRPuntos" id="numTRPuntos">
                        </div>
                      </div>
                </div>
                </div> 
                <div class="card-header">
                <h3 class="card-title"></h3>
                <button type="submit" name="btnAuxiliar" id="btnAuxiliar" value="Registrar" class="btn btn-default float-right"><i class="fas fa-pencil-alt"> Regisrar</i> </button>
              </div>  
              </div>
              <!-- /.card-body -->

            </div>
            </form>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
   <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>

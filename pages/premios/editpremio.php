<?php 
session_start();
  $NombreSesion =  $_SESSION["nombre"]  ;
  $ApellidoSesion =  $_SESSION["apelido"] ;
  $PerfilSesion =  $_SESSION["id_perfil"] ;
  $idSesion =  $_SESSION["id_usuario"] ;
include_once("../../php/libreria.php");
if(empty($_SESSION["id_usuario"])){
  header("refresh:0; url=../form/login.html");
}
  $objconexion  = new Conexion();
  $idconexion   = $objconexion->conectar();
  $objCrud    = new Crud();
  $objUtilidades = new Utilidades();
  $objCrudSesion = new Crud();
  $objCrudSesion->tablas = "perfil_usuario";
  $objCrudSesion->expresion ="*";
  $objCrudSesion->condicion ="id_perfil = '$PerfilSesion ' ";
  $contar=$objCrudSesion->read();
  $arreglo=$objCrudSesion->filas;
  $nomPerfil=$arreglo[0]["perfil"];

 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Editar Premio</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

       <?php 
if($PerfilSesion==1){
include ("../../include/navAdmin.php"); 
}else{
  include ("../../include/navOperario.php");
}
?>
<?php 
    if (isset($_GET['id_premio'])) { 
    $id_busqueda    = $_GET['id_premio'];
    $objCrud->tablas  = "premio";
    $objCrud->expresion = "*";
    $objCrud->condicion = "id_premio = '$id_busqueda'";
    $objCrud->read();
    $ardatos      = $objCrud->filas;

            /*echo "<pre>";
                print_r($ardatos);
              echo "</pre>";*/

    $id         = $ardatos[0]['id_premio'];
    $idEvento       = $ardatos[0]['id_evento'];
    $descripcion      = $ardatos[0]['descripcion_premio'];
   ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Nuevo Premio</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <form action="editpremio.php?id_premio=<?php echo $_GET['id_premio']; ?>" method="POST"">
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-4">
            <div class="input-group-append">
            <label>Premio</label>
            </div>
            <div class="input-group mb-3">
              <input type="text" class="form-control" name="Pre_descripcion" value="<?php echo $descripcion; ?>" placeholder="Premio">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-wine-bottle"></span>
                </div>
              </div>
            </div>
            <div class="input-group-append">
            <label>Evento</label>
            </div>
            <div class="input-group mb-3">
                <select class="form-control" name="lstEvento" id="lstEvento">
                         <option><?php echo $idEvento; ?></option>
                          <?php
                            
                            $objUtilidades->llenar_combo("evento","id_evento,nombre","id_evento",$idconexion);
                          ?>
                </select>
            </div>
            <div class="input-group mb-3">
              <div class="form-group">
                <input  class="btn btn-primary btn-block" type="submit" id="btnActualizar" name="btnActualizar" value="Actualizar" >
              </div>
           </div>
        </div>


          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    </form>
    <?php   

      //Llamar al metodo update para guardar /modificar datos

      if (isset($_POST["btnActualizar"])) {

        $vrPreid=$id;

        $vridEvent  = htmlspecialchars($_POST["lstEvento"]);
        $vrPreDescripcion = htmlspecialchars($_POST["Pre_descripcion"]);

        $objCrud = new Crud();
        $objCrud ->tablas     = "premio";
        $objCrud ->expresion  = "id_Evento = '$vridEvent', descripcion_premio = '$vrPreDescripcion'";
        
        $objCrud ->condicion = "id_premio='$vrPreid'";
        $objCrud ->update();
        $_SESSION['message'] = 'Premio Actualizado';
        $_SESSION['message_type'] = 'primary';
        //include ("alluser.php");
        echo "<a href='allpremio.php' >Verificar</a>";



      }


    ?>
    <!-- /.content -->
  </div>
  <?php 
          
          } 
        ?> 
   <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>

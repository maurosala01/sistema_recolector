<?php 
//inicio de sesion
session_start();
  $NombreSesion =  $_SESSION["nombre"]  ;
  $ApellidoSesion =  $_SESSION["apelido"] ;
  $PerfilSesion =  $_SESSION["id_perfil"] ;
  $idSesion =  $_SESSION["id_usuario"] ;
include_once("../../php/libreria.php");
include_once("../../classes/PHPExcel/IOFactory.php");
$objconexion  = new Conexion();
  $idconexion   = $objconexion->conectar();

if(empty($_SESSION["id_usuario"])){
  header("refresh:0; url=../form/login.html");
}

$objCrudSesion = new Crud();
  $objCrudSesion->tablas = "perfil_usuario";
  $objCrudSesion->expresion ="*";
  $objCrudSesion->condicion ="id_perfil = '$PerfilSesion ' ";
  $contar=$objCrudSesion->read();
  $arreglo=$objCrudSesion->filas;
  $nomPerfil=$arreglo[0]["perfil"];

  //creacion crud de la tabla
  $objCrud               = new Crud();
  $objCrud->tablas       = "usuario";
  $objCrud->expresion    = "*";
  if(isset($_REQUEST["import"])){
    if($_FILES["archivo"]["error"]>0){
        $_SESSION['message'] = 'Error al cargar archivo';
        $_SESSION['message_type'] = 'warning';
      }
    else{
        $permitido= array("text/xls","text/xlsx","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        if(in_array($_FILES["archivo"]["type"], $permitido)){
          $ruta= './files/excel/';
          $arch=$ruta.$_FILES["archivo"]["name"];

          if(!file_exists($ruta)){
            mkdir($ruta, 0777, true);
          }

          if(!file_exists($arch)){

            $resultado= @move_uploaded_file($_FILES["archivo"]["tmp_name"], $arch);
            $objPHPExcel=PHPEXCEL_IOFactory::load($arch);
            $objPHPExcel->setActiveSheetIndex(0);
            $numRows    =$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
            for($i=2 ; $i <= $numRows ; $i++){
              $docExcel       = $objPHPExcel->getActiveSheet()->getCell('A'.$i);
              $nomExcel       = $objPHPExcel->getActiveSheet()->getCell('B'.$i);
              $apeExcel       = $objPHPExcel->getActiveSheet()->getCell('C'.$i);
              $estExcel       = $objPHPExcel->getActiveSheet()->getCell('D'.$i);
              $telExcel       = $objPHPExcel->getActiveSheet()->getCell('E'.$i);
              $conExcel       = $objPHPExcel->getActiveSheet()->getCell('F'.$i);
              $emailExcel     = $objPHPExcel->getActiveSheet()->getCell('G'.$i);
              $perfilAprendiz =3;
              $objCrudExcel   =new crud();
              $objCrud->tablas  = "usuario";
              $objCrud->campos  = "id_perfil, documento, telefono, nombre, apellido, correo,contrasena,genero";
              $objCrud->valores ="'$perfilAprendiz','$docExcel','$telExcel','$nomExcel','$apeExcel','$emailExcel','$conExcel','$estExcel'";
              $objCrud->create($idconexion);
            }

            $_SESSION['message'] = 'Importacion exitosa';
            $_SESSION['message_type'] = 'success';
          }
          else{
            $_SESSION['message'] = 'El archivo ya existe';
            $_SESSION['message_type'] = 'secondary';
          }

        }
        else{
          $_SESSION['message'] = 'archivo no permitido';
          $_SESSION['message_type'] = 'secondary';
        }
        
      }
      

  }

  if(isset($_REQUEST["btnBusqueda"])){
      $vrtextBusqueda = htmlspecialchars($_REQUEST["txtBusqueda"]);
      
          $objCrud->condicion = "documento like '%$vrtextBusqueda%'";  
    }
    $objCrud->ordenamiento = "apellido ASC, nombre ASC";
    $vrcanUsuarios      = $objCrud->read();
    if($vrcanUsuarios == 0){
      $_SESSION['message'] = 'no hay usuarios para consultar';
      $_SESSION['message_type'] = 'primary';
      die();
    }
    $arregloUsuarios = $objCrud->filas;

 ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Usuarios</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="../../plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<script type="text/javascript">
  function comfirmDelete(){
    var respuesta =confirm("Estas seguro que deseas eliminar el/los usuario(s)");
    if(respuesta == true){
      return true;
    }
    else {
      return false;
    }
  }
</script>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  
      <?php 
if($PerfilSesion==1){
include ("../../include/navAdmin.php"); 
}else{
  include ("../../include/navOperario.php");
}
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Lista de Usuarios</h1>
          </div>
        </div>
        <?php if(isset($_SESSION['message'])) {?>

        <div class="alert alert-<?=$_SESSION['message_type'];?> alert-dismissible fade show" role="alert">    <?= $_SESSION['message']?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

      <?php $_SESSION['message'] = null; }?>
      </div>
    </section>
    <section class="content">
      <form name="frmingExcel" id="frmingExcel" method="post" enctype="multipart/form-data" action='<?php echo $_SERVER["PHP_SELF"]; ?>'>
      	<div class="row">
      		<div class="col-md-12">
      			<div class="card card-primary card-outline">
      				<div class="card-header">
      					<h3 class="card-title">Importar</h3>
      				</div>
      				<div class="card-body col-md-6 float-lefl">
      					<input class="form-control border-0" type="file" name="archivo" accept=".xls,.xlsx">
      					<button type="submit" id="submit" name="import" class="btn btn-default" style="margin-top: 12px; margin-left: 12px">
      						<i class="fas fa-upload"></i>
      						Importar
      					</button>
      				</div>
      			</div>
      		</div>
    	</div>
      </form>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- /.col -->
        <div class="col-md-12">
          <div class="card card-primary card-outline">
            <form name="frmBusqueda" id="frmBusqueda" method="get" action='<?php echo $_SERVER["PHP_SELF"]; ?>'>
            <div class="card-header">
              <h3 class="card-title">Usuario</h3>
              <div class="card-tools">
                <div class="input-group input-group-sm">
                  
                    <input type="text" class="form-control" name="txtBusqueda" id="txtBusqueda" placeholder="Documento">
                    <div class="input-group-append">
                      <div class="btn btn-primary" style="border: 0px; padding: 0px">
                        <button type="btnBusqueda" name="btnBusqueda" id="btnBusqueda" style="border: 0px; padding: 0.55rem">
                          <i class="fas fa-search" ></i>
                        </button>
                        
                      </div>
                    </div>
                  </div>
              </div>
              

              <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            </form>
            <div class="card-body p-0">
              <form action="deleteuser.php" method="post">
              <div class="mailbox-controls">
                <!-- Check all button -->
                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="far fa-square"></i>
                </button>
                <div class="btn-group">
                  <button type="submit" name="btnEliminar" title="eliminar" class="btn btn-default btn-sm" onclick="return comfirmDelete()"><i class="far fa-trash-alt"></i></button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fas fa-reply"></i></button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fas fa-share"></i></button>
                </div>
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-sync-alt"></i></button>
                <div class="float-right">
                  1-50/200
                  <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-left"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-right"></i></button>
                  </div>
                  <!-- /.btn-group -->
                </div>
                <!-- /.float-right -->
              </div>

              <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
                  <tbody>
                  <tr>
                    <td>
                    </td>
                    <td class="mailbox-name">Nombre</td>
                    <td class="mailbox-name">Apellido</td>
                    <td class="mailbox-subject">Documento</td>
                    <td class="mailbox-subject">Teléfono</td>
                    <td class="mailbox-subject">Correo</td>
                    <td class="mailbox-subject">Estado</td>
                    <td class="mailbox-subject">Editar</td>
                  </tr>
                    
                    <?php 
                       $vrno = 1;
                       foreach ($arregloUsuarios as $valor) {
                        $vrusuId            = $valor["id_usuario"];
                        $vrusuDocidentidad  = $valor["documento"];
                        $vrusuApellidos     = $valor["apellido"];
                        $vrusuNombres       = $valor["nombre"];
                        $vrusuCorreo        = $valor["correo"];
                        $vrusuTelefono      = $valor["telefono"];
                        $vrusuEstado        = $valor["genero"];
                        
                        if($vrusuId != 1){
                         
                        echo "<tr>";
                        echo "<td>";
                        
                        echo "<input type='checkbox' class='icheck-primary' name='idUsu[]'  value=$vrusuId >";
                       
                        echo "</td>";
                        echo "<td class='mailbox-name'>$vrusuNombres</td>";
                        echo "<td class='mailbox-nam'>$vrusuApellidos</td>";
                        echo "<td class='mailbox-subject'>$vrusuDocidentidad</td>";
                        echo "<td class='mailbox-subject'>$vrusuTelefono</td>";
                        echo "<td class='mailbox-subject'>$vrusuCorreo</td>";
                        echo "<td class='mailbox-subject'>$vrusuEstado</td>";
                        echo "<td class='mailbox-subject'><a title='editar' href='edituser.php?id_usuario=$vrusuId'><i class ='far fa-edit'></i></a></td>";

                        echo "</tr>";
                        
                        $vrno++;
                        }
                      
     }
                     ?>
                  </tr>
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.card-body -->
            <div class="card-footer p-0">
              <div class="mailbox-controls">
                <!-- Check all button -->
                <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="far fa-square"></i>
                </button>
                <div class="btn-group">
                  <button type="submit" name="btnEliminar" title="eliminar" class="btn btn-default btn-sm" onclick="return comfirmDelete()"><i class="far fa-trash-alt"></i></button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fas fa-reply"></i></button>
                  <button type="button" class="btn btn-default btn-sm"><i class="fas fa-share"></i></button>
                </div>
                <!-- /.btn-group -->
                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-sync-alt"></i></button>
                <div class="float-right">
                  1-50/200
                  <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-left"></i></button>
                    <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-right"></i></button>
                  </div>
                  <!-- /.btn-group -->
                </div>
                <!-- /.float-right -->
              </div>
              </form>
            </div>
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- Page Script -->
<script>
  $(function () {
    //Enable check and uncheck all functionality
    $('.checkbox-toggle').click(function () {
      var clicks = $(this).data('clicks')
      if (clicks) {
        //Uncheck all checkboxes
        $('.mailbox-messages input[type=\'checkbox\']').prop('checked', false)
        $('.checkbox-toggle .far.fa-check-square').removeClass('fa-check-square').addClass('fa-square')
      } else {
        //Check all checkboxes
        $('.mailbox-messages input[type=\'checkbox\']').prop('checked', true)
        $('.checkbox-toggle .far.fa-square').removeClass('fa-square').addClass('fa-check-square')
      }
      $(this).data('clicks', !clicks)
    })

    //Handle starring for glyphicon and font awesome
    $('.mailbox-star').click(function (e) {
      e.preventDefault()
      //detect type
      var $this = $(this).find('a > i')
      var glyph = $this.hasClass('glyphicon')
      var fa    = $this.hasClass('fa')

      //Switch states
      if (glyph) {
        $this.toggleClass('glyphicon-star')
        $this.toggleClass('glyphicon-star-empty')
      }

      if (fa) {
        $this.toggleClass('fa-star')
        $this.toggleClass('fa-star-o')
      }
    })
  })
</script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>

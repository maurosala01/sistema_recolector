<?php 
session_start();
  $NombreSesion =  $_SESSION["nombre"]  ;
  $ApellidoSesion =  $_SESSION["apelido"] ;
  $PerfilSesion =  $_SESSION["id_perfil"] ;
  $idSesion =  $_SESSION["id_usuario"] ;
include_once("../../php/libreria.php");
if(empty($_SESSION["id_usuario"])){
  header("refresh:0; url=../form/login.html");
}
  $objconexion  = new Conexion();
  $idconexion   = $objconexion->conectar();
  $objCrud    = new Crud();
  $objUtilidades = new Utilidades();
  $objCrudSesion = new Crud();
  $objCrudSesion->tablas = "perfil_usuario";
  $objCrudSesion->expresion ="*";
  $objCrudSesion->condicion ="id_perfil = '$PerfilSesion ' ";
  $contar=$objCrudSesion->read();
  $arreglo=$objCrudSesion->filas;
  $nomPerfil=$arreglo[0]["perfil"];

 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Editar Usuario</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  

      <?php 
if($PerfilSesion==1){
include ("../../include/navAdmin.php"); 
}else{
  include ("../../include/navOperario.php");
}
?>
    
  <?php 
    if (isset($_GET['id_usuario'])) { 
    $id_busqueda    = $_GET['id_usuario'];
    $objCrud->tablas  = "usuario";
    $objCrud->expresion = "*";
    $objCrud->condicion = "id_usuario = '$id_busqueda'";
    $objCrud->read();
    $ardatos      = $objCrud->filas;

            /*echo "<pre>";
                print_r($ardatos);
              echo "</pre>";*/

    $id         = $ardatos[0]['id_usuario'];
    $perfil       = $ardatos[0]['id_perfil'];
    $documento      = $ardatos[0]['documento'];
    $telefono     = $ardatos[0]['telefono'];
    $nombre       = $ardatos[0]['nombre'];
    $apellido     = $ardatos[0]['apellido'];
    $correo       = $ardatos[0]['correo'];
    $estado       = $ardatos[0]['genero'];
   ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Editar Usuario</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <form action="edituser.php?id_usuario=<?php echo $_GET['id_usuario']; ?>" method="POST">
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-4">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="../../dist/img/avatar04.png"
                       alt="User profile picture">
                </div>
                      <!-- boton seleccionar tipo de perfil -->
                      <div class="row">
                      <div class="form-group col-md-6">
                        <label for="listperfil">Tipo de Perfil</label>
                        <select name="listperfil" id="listperfil" class="form-control">
                          <option><?php echo $perfil; ?></option>
                          <?php
                            
                            $objUtilidades->llenar_combo("perfil_usuario","id_perfil,perfil","id_perfil",$idconexion);
                          ?>
                        </select>
                      </div>
                      <div class="form-group col-md-6">
                        <label>Estado</label>
                        <select name="lstGenero" id="lstGenero" class="form-control">
                          <?php  echo "<option value='$estado'>$estado</option>";  ?>
                          <option value="M">Masculino</option>
                          <option value="F">Femenino</option>
                          
                        </select>
                      </div>
                    </div>
                <input  class="btn btn-primary btn-block" type="submit" id="btnActualizar" name="btnActualizar" value="Actualizar" >
              </div>
              <!-- /.card-body -->
            </div>

            <!-- /.card -->
          </div>
          <div class="col-md-4">
            <div class="input-group mb-3">
              <input type="text" class="form-control" name="Usu_nombre" placeholder="Nombre" value="<?php echo $nombre; ?>">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-user"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="text" class="form-control" name ="Usu_apellido"placeholder="Apellido" value="<?php echo $apellido; ?>">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-users"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
             <input type="" class="form-control" name="Usu_documento" placeholder="Documento" value="<?php echo $documento; ?>">
             <div class="input-group-append">
               <div class="input-group-text">
                 <span class="fas fas fa-id-card"></span>
               </div>
            </div>
       		 </div>

            <div class="input-group mb-3">
             <input type="text" class="form-control" name="Usu_telefono" placeholder="Telefono" value="<?php echo $telefono; ?>">
             <div class="input-group-append">
               <div class="input-group-text">
                 <span class="fas fa-mobile-alt"></span>
               </div>
            </div>
           </div>
           <div class="input-group mb-3">
             <input type="email" class="form-control" name="Usu_correo" placeholder="Email" value="<?php echo $correo; ?>">
             <div class="input-group-append">
               <div class="input-group-text">
                 <span class="fas fa-envelope"></span>
               </div>
            </div>
           </div>
        </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    </form>
    <?php   

      //Llamar al metodo update para guardar /modificar datos

      if (isset($_POST["btnActualizar"])) {

        $vrusuid=$id;

        $vrusuperfil  = htmlspecialchars($_POST["listperfil"]);
        $vrusudocumento = htmlspecialchars($_POST["Usu_documento"]);
        $vrusutelefono  = htmlspecialchars($_POST["Usu_telefono"]);
        $vrusunombre  = htmlspecialchars($_POST["Usu_nombre"]);
        $vrusuapellido  = htmlspecialchars($_POST["Usu_apellido"]);
        $vrusucorreo  = htmlspecialchars($_POST["Usu_correo"]);
        $vrusuestado  = htmlspecialchars($_POST["lstGenero"]);

        $objCrud = new Crud();
        $objCrud ->tablas     = "usuario";
        $objCrud ->expresion  = "id_perfil = '$vrusuperfil', documento = '$vrusudocumento', telefono = '$vrusutelefono', nombre = '$vrusunombre', apellido = '$vrusuapellido', correo = '$vrusucorreo', genero = '$vrusuestado'";
        //$objCrud ->expresion ="'Doc_id'";
        
        $objCrud ->condicion = "id_usuario='$vrusuid'";
        $objCrud ->update();
        $_SESSION['message'] = 'Usuario actualizado!!!';
        $_SESSION['message_type'] = 'primary';
        //include ("alluser.php");
        echo "<a href='alluser.php' >Verificar</a>";



      }


    ?>

    <!-- /.content -->
  </div>
  <?php 
          
          } 
        ?> 
   <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>

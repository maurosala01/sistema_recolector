<?php 
session_start(); 
  $NombreSesion =  $_SESSION["nombre"]  ;
  $ApellidoSesion =  $_SESSION["apelido"] ;
  $PerfilSesion =  $_SESSION["id_perfil"] ;
  $idSesion =  $_SESSION["id_usuario"] ;
  include_once("../../php/libreria.php");
  if(empty($_SESSION["id_usuario"])){
  header("refresh:0; url=../form/login.html");
}
  $objconexion  = new Conexion();
  $idconexion   = $objconexion->conectar();
  $objCrud    = new Crud();
  $objUtilidades = new Utilidades();
  $vridUsuario   = $objUtilidades->consecutivo("usuario","id_usuario",$idconexion);
  $objCrudSesion = new Crud();
  $objCrudSesion->tablas = "perfil_usuario";
  $objCrudSesion->expresion ="*";
  $objCrudSesion->condicion ="id_perfil = '$PerfilSesion ' ";
  $contar=$objCrudSesion->read();
  $arreglo=$objCrudSesion->filas;
  $nomPerfil=$arreglo[0]["perfil"];

  $vrusuNombre   = "";
  $vrusuApellido = "";
  $vrusuCorreo   = "";
  $vrusuContrasena = "";
  if(isset($_REQUEST["btnAuxiliar"])){
       $vrusuPerfil        = htmlspecialchars($_REQUEST["lstidPerfil"]); 
       $vrusuDocumento     = htmlspecialchars($_REQUEST["txtusuDocumento"]);
       $vrusuTelefono      = htmlspecialchars($_REQUEST["txtusuTelefono"]);
       $vrusuNombre        = htmlspecialchars($_REQUEST["txtusuNombre"]);
       $vrusuApellido      = htmlspecialchars($_REQUEST["txtusuApellido"]);
       $vrusuCorreo        = htmlspecialchars($_REQUEST["txtusuCorreo"]);
       $vrusuContrasena    = htmlspecialchars($_REQUEST["txtusuContrasena"]);
       $vrusuEstado        = htmlspecialchars($_REQUEST["lstGenero"]); 
       $hashPassword       = md5( $vrusuContrasena);
       $objCrud            =  new Crud();
       $objCrud->tablas    = "usuario";
       $objCrud->expresion = "*";
       $objCrud->condicion = "documento='$vrusuDocumento'";
       $vrcanusuarios      = $objCrud->read($idconexion);
       if($vrcanusuarios>0){
        $_SESSION['message'] = 'El usuario ya existe';
             $_SESSION['message_type'] = 'warning';
       } else {
         $objCrud1          = new Crud();
         $objCrud1->tablas  = "usuario";
         $objCrud1->campos  = "id_usuario, id_perfil, documento, telefono, nombre, apellido, correo, contrasena, genero";
         $objCrud1->valores ="'$vridUsuario','$vrusuPerfil','$vrusuDocumento','$vrusuTelefono','$vrusuNombre','$vrusuApellido','$vrusuCorreo','$hashPassword','$vrusuEstado'";
         $objCrud1->create();
         $_SESSION['message'] = 'Usuario Registrado!!!';
         $_SESSION['message_type'] = 'success';
         

       }
    }
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Nuevo Usuario</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  

     <?php 
if($PerfilSesion==1){
include ("../../include/navAdmin.php"); 
}else{
  include ("../../include/navOperario.php");
}
?>
    

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Nuevo Usuario</h1>
          </div>
        </div>
        <?php if(isset($_SESSION['message'])) {?>

        <div class="alert alert-<?=$_SESSION['message_type'];?> alert-dismissible fade show" role="alert">    <?= $_SESSION['message']?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

      <?php $_SESSION['message'] = null; }?>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <form  name="frmingUsuarios" id="frmingUsuarios" method="post"  action='<?php echo $_SERVER["PHP_SELF"]; ?>'>
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-4">
            <div class="input-group mb-3">
              <input type="text" class="form-control" name="txtusuNombre" id="txtusuNombre" placeholder="Nombre">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-user"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
              <input type="text" class="form-control" name="txtusuApellido" id="txtusuApellido" placeholder="Apellido">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-users"></span>
                </div>
              </div>
            </div>
            <div class="input-group mb-3">
             <input type="number" class="form-control" name="txtusuDocumento" id="txtusuDocumento" placeholder="Documento">
             <div class="input-group-append">
               <div class="input-group-text">
                 <span class="fas fas fa-id-card"></span>
               </div>
            </div>
       		 </div>
           <div class="input-group mb-3">
                  <select class="form-control" name="lstGenero" id="lstGenero">
                    <option value="">Genero</option>
                    <option value="M">Masculino</option>
                    <option value="F">Femenino</option>
                  </select>
                  <div class="input-group-append">
               <div class="input-group-text">
                 <span class="fas fas fa-venus-mars"></span>
               </div>
            </div>
            </div>

           <div class="input-group mb-3">
             <input type="tel" class="form-control" name="txtusuTelefono" id="txtusuTelefono" placeholder="Número de Teléfono" pattern="[0-9]{10}">
             <div class="input-group-append">
               <div class="input-group-text">
                 <span class="fas fa-mobile-alt"></span>
               </div>
            </div>
           </div>
           <div class="input-group mb-3">
             <input type="email" class="form-control" name="txtusuCorreo" id="txtusuCorreo" placeholder="E-mail">
             <div class="input-group-append">
               <div class="input-group-text">
                 <span class="fas fa-envelope"></span>
               </div>
            </div>
           </div>
           <div class="input-group mb-3">
             <input type="password" class="form-control" name="txtusuContrasena" id="txtusuContrasena" placeholder="Password">
             <div class="input-group-append">
               <div class="input-group-text">
                 <span class="fas fa-lock"></span>
               </div>
             </div>
           </div>
           <div class="input-group mb-3">
             <input type="password" class="form-control" name="txtusuContrasena" id="txtusuContrasena" placeholder="Retype password">
             <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
             </div>
          </div>
        </div>

          <div class="col-md-4">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="../../dist/img/logo.png"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center"><?php echo $NombreSesion." ".$ApellidoSesion;?></h3>
                      
                      <!-- boton seleccionar tipo de perfil -->

                      <div class="form-group">
                        <label for="lstidPerfil">Tipo de Perfil</label>
                        <select class="form-control" name="lstidPerfil" id="lstidPerfil">
                          <option value="">Sin seleccionar</option>
                          <?php
                              $objUtilidades->llenar_combo("perfil_usuario","id_perfil,perfil","id_perfil",$idconexion);
                           ?>
                        </select>
                      </div>
                <button type="submit" class="btn btn-primary btn-block" name="btnAuxiliar" id="btnAuxiliar" value="Registrar">Registrar</button>
              </div>
              <!-- /.card-body -->
            </div>

            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    </form>
    <!-- /.content -->
  </div>
   <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>

<?php 
session_start();
  $NombreSesion =  $_SESSION["nombre"]  ;
  $ApellidoSesion =  $_SESSION["apelido"] ;
  $PerfilSesion =  $_SESSION["id_perfil"] ;
  $idSesion =  $_SESSION["id_usuario"] ;
  include_once("../../php/libreria.php");
  if(empty($_SESSION["id_usuario"])){
  header("refresh:0; url=../form/login.html");
}

  $objconexion  = new Conexion();
  $idconexion   = $objconexion->conectar();

  $objCrudSesion = new Crud();
  $objCrudSesion->tablas = "perfil_usuario";
  $objCrudSesion->expresion ="*";
  $objCrudSesion->condicion ="id_perfil = '$PerfilSesion ' ";
  $contar=$objCrudSesion->read();
  $arreglo=$objCrudSesion->filas;
  $nomPerfil=$arreglo[0]["perfil"];

  $objUtilidades = new Utilidades();
  $vridReciclaje   = $objUtilidades->consecutivo("registrar_reciclaje","id_reciclaje",$idconexion);

  $vrdocUsu      ="";
  $vridEve      ="";
  $vrCantidad   =0;
  if(isset($_REQUEST["btnAuxiliar"])){
    $vrdocUsu    = htmlspecialchars($_REQUEST["doc_usu"]);
    $vridEve    = htmlspecialchars($_REQUEST["eve_registro"]);
    $vrCantidad = htmlspecialchars($_REQUEST["recolecta"]);

    //verifica si existe el ususario
    $objUsu             = new crud();
    $objUsu->tablas     ="usuario";
    $objUsu->expresion  ="*";
    $objUsu->condicion  ="documento='$vrdocUsu'";
    $vrcanusuarios      = $objUsu->read($idconexion);
    if(empty($vrcanusuarios)){
      $_SESSION['message'] = 'Este documento no pertenece a ningun usuario';
      $_SESSION['message_type'] = 'warning';
       }
    else{

      $usuArreglo       =$objUsu->filas;
      $vridUsu          =$usuArreglo[0]["id_usuario"];

      //buscamos el tipo de reclaje del evento 
      $objEve              = new crud();
      $objEve->tablas      ="evento";
      $objEve->expresion   ="*";
      $objEve->condicion   ="id_evento='$vridEve'";
      $eve                 =$objEve->read($idconexion);
      $eveArreglo          =$objEve->filas;
      $tipoReciclaje       =$eveArreglo[0]["id_tipo_reciclaje"];

      //extraemos el multiplicador del tipo de reciclaje
      $objTR              = new crud();
      $objTR->tablas      ="tipo_reciclaje";
      $objTR->expresion   ="*";
      $objTR->condicion   ="id_tipo_reciclaje='$tipoReciclaje'";
      $tipoR              =$objTR->read($idconexion);
      $TRarreglo          =$objTR->filas;
      $multiplicador      =$TRarreglo[0]["puntos_botella"];

      $vrpuntos           =$vrCantidad*$multiplicador;

      //buscamos registros previos para el acumulado
      $objRegPrev              = new crud();
      $objRegPrev->tablas      ="registrar_reciclaje";
      $objRegPrev->expresion   ="id_evento,id_usuario,puntos_totales";
      $objRegPrev->condicion   ="id_usuario='$vridUsu' AND id_evento='$vridEve'";
      $regPrev                 =$objRegPrev->read($idconexion);
      if(isset($regPrev)){
        $regPrevArreglo          =$objRegPrev->filas;
        if($regPrevArreglo!=0){
        foreach ($regPrevArreglo as $vmax) {

          # code...
          $acumulado              =$vmax["puntos_totales"];
        }
        }
        else{
        $acumulado=0;
      }

      }
      
      $vrpuntosTotales=$acumulado+$vrpuntos;

      //registramos los puntos
      $objCrud          = new Crud();
      $objCrud->tablas  = "registrar_reciclaje";
      $objCrud->campos  = "id_reciclaje, id_evento, id_usuario, cantidad, puntos,puntos_totales";
      $objCrud->valores ="'$vridReciclaje','$vridEve','$vridUsu','$vrCantidad','$vrpuntos','$vrpuntosTotales'";
      $objCrud->create();
      $_SESSION['message'] = 'Registro guardado con exito !!!';
      $_SESSION['message_type'] = 'success';

    }
  }


 ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Puntos</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
    <!-- Right navbar links -->
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
 

      <!-- Sidebar Menu -->

<?php 
if($PerfilSesion==1){
include ("../../include/navAdmin.php"); 
}else{
  include ("../../include/navOperario.php");
}
?>
    
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        
        <div class="row mb-2">
          
          <div class="col-sm-6">

            <h1>Puntos</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
      <?php if(isset($_SESSION['message'])) {?>

        <div class="alert alert-<?=$_SESSION['message_type'];?> alert-dismissible fade show" role="alert">    <?= $_SESSION['message']?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

      <?php $_SESSION['message'] = null; }?>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="../../dist/img/logo.png"
                       alt="User profile picture">
                </div>
                <h3 class="profile-username text-center"><?php echo $NombreSesion ?></h3>
                <h4 class="profile-username text-center"><?php echo $nomPerfil ?></h4>
              </div>
              <!-- /.card-body -->
            </div>

            <!-- /.card -->
          </div>
          <form  name="ingPuntos" id="ingPuntos" method="post"  action='<?php echo $_SERVER["PHP_SELF"]; ?>'>
          <div class="col-md-12">
             <div class="card card-primary card-outline">
              <!-- /.card-header -->
              <div class="card-body">
                <div class="form-group m-0">
                    <div class="row">
                      <div class="col-md-6">

                      	<div class="form-group">
                       	    <label>Identificación:</label>
							<input class="form-control" name="doc_usu" type="text">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                       	 <label>Evento:</label>
                       	 <select class="form-control" name="eve_registro">
                        	  <option value="">Sin seleccionar</option>
                            <?php
                                $objUtilidades->llenar_combo("evento","id_evento,nombre","id_evento",$idconexion);
                             ?>
                        	</select>
                        </div>
                    </div>
                	</div>
                </div>
                <div class="form-group m-0">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                       	 <label>Unidades de Reciclaje:</label>
							<input class="form-control" name="recolecta" type="number">
                        </div>
                    </div>
                </div>
                </div> 
                <div class="card-header">
                <h3 class="card-title"></h3>
                <button type="submit" name="btnAuxiliar" id="btnAuxiliar" class="btn btn-default float-right"><i class="fas fa-pencil-alt"></i> Asignar Puntos</button>
              </div>  
              </div>
              <!-- /.card-body -->

            </div>
            </form>
          <!-- /.col --
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
   <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>

<?php 
  session_start();
  $NombreSesion =  $_SESSION["nombre"]  ;
  $ApellidoSesion =  $_SESSION["apelido"] ;
  $PerfilSesion =  $_SESSION["id_perfil"] ;
  $idSesion =  $_SESSION["id_usuario"] ;
  include_once("../../php/libreria.php");
  
  if(empty($_SESSION["id_usuario"])){
  header("refresh:0; url=../form/login.html");
}

  $objconexion  = new Conexion();
  $idconexion   = $objconexion->conectar();

  $objCrudSesion = new Crud();
  $objCrudSesion->tablas = "perfil_usuario";
  $objCrudSesion->expresion ="*";
  $objCrudSesion->condicion ="id_perfil = '$PerfilSesion ' ";
  $contar=$objCrudSesion->read();
  $arreglo=$objCrudSesion->filas;
  $nomPerfil=$arreglo[0]["perfil"];


  $objCrud    = new Crud();
  $objUtilidades = new Utilidades();
   $vridEvento = $objUtilidades->consecutivo("evento","id_evento",$idconexion);
   $vrEventoNombre= "";
    $vrEventoLugar= "";
    $vrEventoHInicio="";
    $vrEventoHFinal="";
    $vrEventoFecha="";
    $vrEventoObservaciones= "";
  if(isset($_REQUEST["btnAuxiliar"])){
       //$vridUsuario           = htmlspecialchars($_REQUEST["lstidUsuario"]);
      $vridTipoReciclaje      = htmlspecialchars($_REQUEST["lstidTipoReciclaje"]);
      $vrEventoNombre         = htmlspecialchars($_REQUEST["txtEventoNombre"]);
      $vrEventoLugar          = htmlspecialchars($_REQUEST["txtEventoLugar"]);
      $vrEventoHInicio        = htmlspecialchars($_REQUEST["hrEventoInicio"]);
      $vrEventoHFinal         = htmlspecialchars($_REQUEST["hrEventoFinal"]);
      $vrEventoFechaI        = htmlspecialchars($_REQUEST["dateEventoFechaI"]);
      $vrEventoFechaF        = htmlspecialchars($_REQUEST["dateEventoFechaF"]);
      $vrEventoObservaciones      = htmlspecialchars($_REQUEST["txtEventoObservaciones"]);

      //crgar imagen
      if($_FILES["archivo"]["error"]>0){
        $_SESSION['message'] = 'error al cargar archivo';
        $_SESSION['message_type'] = 'warning';
      }
      else{
        $permitido= array("image/jpg","image/png","image/jpeg");
        if(in_array($_FILES["archivo"]["type"], $permitido)){
          $ruta= './files/img/'.$vridEvento.'/';
          $arch=$ruta.$_FILES["archivo"]["name"];

          if(!file_exists($ruta)){
            mkdir($ruta, 0777, true);
          }

          if(!file_exists($arch)){

            $resultado= @move_uploaded_file($_FILES["archivo"]["tmp_name"], $arch);
            $objCrud          = new Crud();
            $objCrud->tablas  = "evento";
            $objCrud->campos  = "id_evento, id_usuario, id_tipo_reciclaje, nombre, lugar, h_inicio,h_final,fecha_i,fecha_f,observaciones";
            $objCrud->valores ="'$vridEvento','$idSesion','$vridTipoReciclaje','$vrEventoNombre','$vrEventoLugar','$vrEventoHInicio','$vrEventoHFinal','$vrEventoFechaI','$vrEventoFechaF','$vrEventoObservaciones'";
            $objCrud->create($idconexion);
             $_SESSION['message'] = 'Evento Registrado!!!';
             $_SESSION['message_type'] = 'success';

          }
          else{
            $_SESSION['message'] = 'archivo ya existe';
            $_SESSION['message_type'] = 'secondary';
          }

        }
        else{
          $_SESSION['message'] = 'archivo no permitido';
          $_SESSION['message_type'] = 'secondary';
        }
      }

      
    }
 ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Nuevo Evento</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- summernote -->
  <link rel="stylesheet" href="../../plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  
<?php 
if($PerfilSesion==1){
include ("../../include/navAdmin.php"); 
}else{
  include ("../../include/navOperario.php");
}
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Nuevo Evento</h1>
          </div>
        </div>
        <?php if(isset($_SESSION['message'])) {?>

        <div class="alert alert-<?=$_SESSION['message_type'];?> alert-dismissible fade show" role="alert">    <?= $_SESSION['message']?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

      <?php $_SESSION['message'] = null; }?>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile" style="padding: 0px;">
                <div class="text-center" style="background: url();">
                  <img class="profile-user-img img-fluid img-circle"
                       src="../../dist/img/logo.png"
                       alt="User profile picture" style="margin-bottom: 40px;margin-top: 40px">
                </div>

                <h2 class="profile-username text-center"> <?php echo $NombreSesion." ".$ApellidoSesion;?> </h2>
                <h6 class="text-center"><?php echo $nomPerfil ?></h6>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->

            <!-- /.card -->
          </div> 
          <!-- /.col -->
          <form  name="frmingEvento" id="frmingEvento" method="post" enctype="multipart/form-data" action='<?php echo $_SERVER["PHP_SELF"]; ?>'>
          <div class="col-md-13"> 
             <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">Evento</h3>
                <button type="submit" class="btn btn-default float-right" name="btnAuxiliar" id="btnAuxiliar"><i class="fas fa-pencil-alt"></i> Publicar</button>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="form-group">
                  <label>Título</label>
                  <input class="form-control"  name="txtEventoNombre" id="txtEventoNombre" placeholder="Título:">
                </div>
                <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Lugar</label>
                          <input class="form-control" name="txtEventoLugar" id="txtEventoLugar" placeholder="Lugar:">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Tipo de Reciclaje</label>
                          <select class="form-control" name="lstidTipoReciclaje" id="lstidTipoReciclaje">
                            <option >Sin seleccionar</option>
                            <?php
                                $objUtilidades->llenar_combo("tipo_reciclaje","id_tipo_reciclaje,nombre","id_tipo_reciclaje",$idconexion);
                             ?> 
                         </select>
                          
                      </div>
                    </div>
                </div>
                </div>
                <div class="form-group">
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Fecha de Inicio</label>
                          <input class="form-control" type="date" name="dateEventoFechaI" id="dateEventoFechaI">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Fecha de fin</label>
                          <input class="form-control" type="date"type="date" name="dateEventoFechaF" id="dateEventoFechaF">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Hora Inicio</label>
                          <input class="form-control" type="time" name="hrEventoInicio" id="hrEventoInicio">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Hora Fin</label>
                          <input class="form-control" type="time" name="hrEventoFinal" id="hrEventoFinal">
                      </div>
                    </div>
                </div>
                </div>
                <div class="form-group">
                    <input class="form-control" name="txtEventoObservaciones" id="txtEventoObservaciones" placeholder="Descripción...">

                    <div class="col-md-8">
                        <div class="form-group">
                          <label for="archivo" class="pt-4">Subir imágen</label>
                          <input  type="file" class="form-control" id="archivo" name="archivo" accept="image/*">
                        </div>
                      </div>

                </div>
                
              </div>
              <!-- /.card-body -->

            </div>

            </div>
          </div>
          </form>
          <!-- /.col -->
        </div>

        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- Summernote -->
<script src="../../plugins/summernote/summernote-bs4.min.js"></script>
<!-- Page Script -->
<script>
  $(function () {
    //Add text editor
    $('#compose-textarea').summernote()
  })
</script>
</body>
</html>

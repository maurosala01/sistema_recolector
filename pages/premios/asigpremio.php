<?php 
session_start();
  $NombreSesion =  $_SESSION["nombre"]  ;
  $ApellidoSesion =  $_SESSION["apelido"] ;
  $PerfilSesion =  $_SESSION["id_perfil"] ;
  $idSesion =  $_SESSION["id_usuario"] ;
  include_once("../../php/libreria.php");
  if(empty($_SESSION["id_usuario"])){
  header("refresh:0; url=../form/login.html");

   $objConexion   = new Conexion(); 
  $idconexion    = $objConexion->conectar();
}
  $objCrudSesion = new Crud();
  $objCrudSesion->tablas = "perfil_usuario";
  $objCrudSesion->expresion ="*";
  $objCrudSesion->condicion ="id_perfil = '$PerfilSesion ' ";
  $contar=$objCrudSesion->read();
  $arreglo=$objCrudSesion->filas;
  $nomPerfil=$arreglo[0]["perfil"];


  $objUtilidades    =new Utilidades();
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Asignar Premio</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php 
include ("../../include/navAdmin.php");
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Asignar Premio</h1>
          </div>
        </div>
        

      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      
      <div class="container-fluid">
        <form name="frmAsigPremio" id="frmAsigPremio" method="post"  action='<?php echo $_SERVER["PHP_SELF"]; ?>'>
        <div class="row">
          
          <div class="col-md-4">
            <div class="input-group-append">
            <label>Evento</label>
            </div>
            <div class="input-group mb-3">
                <select class="form-control" name="lstEvento" id="lstEvento">
                          <option value="">Sin seleccionar</option>
                          <?php
                              $objUtilidades->llenar_combo("evento","id_evento,nombre","id_evento",$idconexion);
                           ?>
                </select>
            </div>
          </div>
          <div class="col-md-2">

            <div class="input-group-append">
            <label>N° Premios</label>
            </div>
            <div class="input-group mb-3">
                <input type="number" name="nPremios" class="form-control" placeholder="Número de premios">
            </div>
          </div>
          <div class="col-md-3">
            <div class="input-group-append">
            <label>Premio</label>
            </div>
          <div class="input-group mb-2">
             <select class="form-control" name="regis" id="regis">
                       <option value="">Premio</option>
                       <?php
                 $objUtilidades->llenar_combo("premio","descripcion_premio","id_premio",$idconexion);
              ?>

             </select>
          </div>
          </div>
          <div class="col-md-2 pt-2">
                <button type="submit" name="btnAuxiliar" id="btnAuxiliar" value="" class="btn btn-default float-left mt-4"><i class="fas fa-pencil-alt"></i> Registrar</button>
          </div>
          <!-- /.col -->
        </div>
        </form>
         
        <div class="row">
         
        <?php if(isset($_REQUEST["btnAuxiliar"])){
          
          $idBusEve=        htmlspecialchars($_REQUEST["lstEvento"]);
          $numPremios=   htmlspecialchars($_REQUEST["nPremios"]);
          $objGanadores =new crud();
          $objGanadores->tablas    ="registrar_reciclaje";
          $objGanadores->expresion    ="*";
          $objGanadores->condicion   ="id_evento= '$idBusEve'";
          $objGanadores->ordenamiento ="puntos_totales DESC, id_usuario ASC";
          $vrleerRanking               =$objGanadores->read();
          $arregloPuntos = $objGanadores->filas;
          $objConexion   = new Conexion(); 
          $idconexion    = $objConexion->conectar();

          $ban=0;
          $arregloRep=array();
          $reg=0;
           
          foreach ($arregloPuntos as $valor1) {
                      # code...
                      $idEvento              =$valor1["id_evento"];
                      $idUsuario             =$valor1["id_usuario"];
                      $puntosT           =$valor1["puntos_totales"];
                      $objEve              = new crud();
                          $objEve->tablas      ="evento";
                          $objEve->expresion   ="*";
                          $objEve->condicion   ="id_evento='$idEvento'";
                          $eve                 =$objEve->read();
                          $eveArreglo          =$objEve->filas;
                          $nomEve          =$eveArreglo[0]["nombre"];
                        $objUsu             = new crud();
                         $objUsu->tablas     ="usuario";
                         $objUsu->expresion  ="*";
                         $objUsu->condicion  ="id_usuario='$idUsuario'";
                         $vrcanusuarios      = $objUsu->read();
                         $usuArreglo       =$objUsu->filas;
                         $vrnombreusu         =$usuArreglo[0]["nombre"];
                         $vrapeusu      =$usuArreglo[0]["apellido"];




                    $ban=$ban+1;

                    if(in_array($idUsuario, $arregloRep)){ $ban=$ban-1;}
                    else{
                      if($ban<=$numPremios) {?>

                      <div class="col-md-12">
                      <div class="input-group-append">
                      <label>Usuario</label>
                      </div>
                      <div class="input-group mb-0">
                        <h5 class="mb-0"><?php echo $vrnombreusu." ".$vrapeusu; ?></h5>
                      </div>
                      <div class="input-group mb-0">
                        <p class="mb-0"><?php echo "Puntos: ".$puntosT; ?></p>
                     </div>
                     <div class="input-group mb-0">
                        <?php echo "<input id='limiteArreglo' type='hidden' name='limiteArreglo' value='$numPremios'  />";  ?>
                     </div>
                     <div class="input-group mb-0">
                        <p class="mb-0">Posicion: <?php echo $ban; ?></p>
                     </div>
                    </div>
                    
      
                    
<?php   
        $idPrem=        htmlspecialchars($_REQUEST["regis"]);        
        $objRegistro= new crud();
        $objRegistro->tablas="registrar_premio";
        $vridRank = $objUtilidades->consecutivo("registrar_premio","id_ranking",$idconexion);
        $objRegistro->campos  = "id_ranking, id_usuario, id_premio,puntaje_total,posicion";
        $objRegistro->valores ="'$vridRank','$idUsuario','$idPrem','$puntosT','$ban'";
        $objRegistro->create($idconexion); 
        $_SESSION['message'] = 'se han asignado '.$numPremios.' Premio(s)!!!';
        $_SESSION['message_type'] = 'secondary';          
                $arregloRep[$ban-1]=$idUsuario;
                 }

}
                } ?>
         
        
          <?php if(isset($_SESSION['message'])) {?>

        <div class="alert alert-<?=$_SESSION['message_type'];?> alert-dismissible fade show" role="alert">    <?= $_SESSION['message']?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

      <?php $_SESSION['message'] = null; }?>
          
          <!-- /.col -->
        
      <?php } ?>

     </div>
      
        <!-- /.row -->
      </div><!-- /.container-fluid -->
      
    </section>
    
    <!-- /.content -->
  </div>
  
   <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script language="javascript" src="../../plugins/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript">
</script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>

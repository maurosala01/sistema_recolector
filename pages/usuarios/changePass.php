<?php 
session_start();
  $NombreSesion =  $_SESSION["nombre"]  ;
  $ApellidoSesion =  $_SESSION["apelido"] ;
  $PerfilSesion =  $_SESSION["id_perfil"] ;
  $idSesion =  $_SESSION["id_usuario"] ;
  $pass=$_SESSION["contrasena"];
include_once("../../php/libreria.php");
if(empty($_SESSION["id_usuario"])){
  header("refresh:0; url=../form/login.html");
}

$objCrudSesion = new Crud();
  $objCrudSesion->tablas = "perfil_usuario";
  $objCrudSesion->expresion ="*";
  $objCrudSesion->condicion ="id_perfil = '$PerfilSesion ' ";
  $contar=$objCrudSesion->read();
  $arreglo=$objCrudSesion->filas;
  $nomPerfil=$arreglo[0]["perfil"];
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cambio contraseña</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
       <?php 
if($PerfilSesion==1){
include ("../../include/navAdmin.php"); 
}else{
  include ("../../include/navOperario.php");
}
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Cambiar Contraseña</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->

    <div class="col-sm-6">
       <div class="card">
          <div class="card-body login-card-body">
            <p class="login-box-msg">Ingrese la contraseña</p>

            <form action="changePass.php" method="post">
              <div class="input-group mb-3">
                <input type="password" class="form-control" name="actual" placeholder="Contraseña Actual">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                  </div>
                </div>
              </div>
              <div class="input-group mb-3">
                <input type="password" class="form-control" name="nueva"  placeholder="Nueva Contraseña">
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <button type="submit" name="resPass" class="btn btn-primary btn-block">Cambiar</button>
                </div>
                <!-- /.col -->
              </div>
            </form>

            <p class="mt-3 mb-1">
              <a href="editperfil.php">Volver</a>
            </p>
          </div>
    <!-- /.login-card-body -->
        </div>
      </div>
      <?php   

      //Llamar al metodo update para guardar /modificar datos

      if (isset($_POST["resPass"])) {

        $vrusuid=$idSesion;

        $contrasena = htmlspecialchars($_POST["actual"]);
        $newContrasena  = htmlspecialchars($_POST["nueva"]);

        $cifrado= md5($contrasena);
        $cifradoN=md5($newContrasena);
       
        if($pass==$cifrado){
            $objCrud = new Crud();
            $objCrud ->tablas     = "usuario";
            $objCrud ->expresion  = " contrasena = '$cifradoN'";
        
            $objCrud ->condicion = "id_usuario='$vrusuid'";
            $objCrud ->update();
            echo "Contraseña actualizada <br>";
            session_destroy();
            //include ("alluser.php");
            echo "<a href='../../php/salir.php' >Salir</a>";

          }else{

            echo "Contraseña Incorrecta <br>";
            echo "<a href='editperfil.php' >Regresar</a>";

          }

        
      }


    ?>

    <!-- /.content -->
  </div>
   <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>

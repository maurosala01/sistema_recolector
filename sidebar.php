				<!-- Sidebar -->
					<section id="sidebar">

						<!-- Intro -->
							<section id="intro">
								<a href="#" class="logo"><img src="dist/img/logo.png" alt="" /></a>
								<header>
									<h2>Sistema recolector de materiales reciclables </h2>
									<p>Tu eres el cambio...</p>
								</header>
							</section>

						<!-- Posts List -->
							<section>
								<ul class="posts">
									<li>
										<article>
												<form class="search" method="get" action="#">
													<input type="text" name="query" placeholder="Buscar" />
												</form>
										</article>
									</li>
									<li>
										<?php 
										$vrno =0;
										$arregloRepetidos=array();
										foreach ($arregloPuntos as $valor1) {
											# code...
											$idEvento              =$valor1["id_evento"];
											$idUsuario             =$valor1["id_usuario"];
											$puntosT		       =$valor1["puntos_totales"];
											$objEve              = new crud();
										      $objEve->tablas      ="evento";
										      $objEve->expresion   ="*";
										      $objEve->condicion   ="id_evento='$idEvento'";
										      $eve                 =$objEve->read();
										      $eveArreglo          =$objEve->filas;
										      $nomEve		       =$eveArreglo[0]["nombre"];
										    $objUsu             = new crud();
											   $objUsu->tablas     ="usuario";
											   $objUsu->expresion  ="*";
											   $objUsu->condicion  ="id_usuario='$idUsuario'";
											   $vrcanusuarios      = $objUsu->read();
											   $usuArreglo       =$objUsu->filas;
											   $vrnombreusu         =$usuArreglo[0]["nombre"];
											   $vrapeusu			=$usuArreglo[0]["apellido"];
											   $vrEstaUsu			=$usuArreglo[0]["genero"];


										if(in_array($idUsuario, $arregloRepetidos)){}
											else{

										 ?>
										<article>
											<header>
												<h3><?php echo $vrnombreusu." ".$vrapeusu; ?></h3>
												<p class="sinespa"><?php echo $puntosT; ?></p>
												<p class="sinespa interline">Evento: <a <?php echo "href='evento.php?id_evento=$vridEvento'"; ?> ><?php echo $nomEve; ?></a></p>
											</header>
											<?php 
											if($vrEstaUsu  =='M'){ ?>
											<img class="image" src="dist/img/avat02.jpg" alt="" />
											<?php 
											}else{?>
											<img class="image" src="dist/img/avat01.jpg" alt="" />
											<?php 
											}
											 ?>
										</article>
									</li>
									
									<?php 

											   $arregloRepetidos=array(
											   "arrayidUsu" => $idUsuario,
											   "arrayidEvento" => $idEvento);

								}
									} ?>
								</ul>
							</section>

						<!-- About -->
						

						<!-- Footer -->
							<section id="footer">
								<ul class="icons">
									<li><a href="https://twitter.com/SistemaRecolec1?s=08" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
									<li><a href="https://www.facebook.com/Sistema-Recolector-de-Materiales-Reciclables-105152521107868/" class="icon brands fa-facebook-f"><span class="label">Facebook</span></a></li>
									<li><a href="https://www.instagram.com/sistemarecolector/?hl=es-la" class="icon brands fa-instagram"><span class="label">Instagram</span></a></li>
									
								</ul>

							</section>

					</section>

								</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>
<?php 
session_start();
  $NombreSesion =  $_SESSION["nombre"]  ;
  $ApellidoSesion =  $_SESSION["apelido"] ;
  $PerfilSesion =  $_SESSION["id_perfil"] ;
  $idSesion =  $_SESSION["id_usuario"] ;
include_once("php/libreria.php");
if(empty($_SESSION["id_usuario"])){
  header("refresh:0; url=pages/form/login.html");
}
$objConexion   = new Conexion(); 
  $idconexion    = $objConexion->conectar();

  $objUtilidades = new Utilidades();

  


$objCrudSesion = new Crud();
  $objCrudSesion->tablas = "perfil_usuario";
  $objCrudSesion->expresion ="*";
  $objCrudSesion->condicion ="id_perfil = '$PerfilSesion ' ";
  $contar=$objCrudSesion->read();
  $arreglo=$objCrudSesion->filas;
  $nomPerfil=$arreglo[0]["perfil"];



 ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Panel de Control Operario</title>
<!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>
  <a href="php/salir.php" class="btn btn-default btn-sm" title="salir" style="position: absolute;right: 0px">
   <i class="fas fa-power-off"></i>
  </a>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="operario.php" class="brand-link">
      <img src="dist/img/operario.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Panel de Control</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
        <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item has-treeview ">
              <a href="#" class="nav-link pl-0 pb-0">
                  <h5>
                  <?php echo $NombreSesion." ".$ApellidoSesion; ?>
                  </h5>
                </a>
                <ul class="nav nav-treeview">
                 <li class="nav-item">
                    <a href="pages/usuarios/editperfil.php" class="nav-link">
                      <i class="fas fa-edit nav-icon"></i>
                    <p>Editar</p>
                   </a>
                 </li>
               </ul>
             </li>
            </ul>
          </nav>
          <a><?php echo $nomPerfil ?></a>
        </div>
      </div>
      <!-- Sidebar Menu -->
     <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="operario.php" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Escritorio
               </p>
            </a>
          </li>
          
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Eventos
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/evento/newevent.php" class="nav-link">
                  <i class="fas fa-edit nav-icon"></i>
                  <p>Nuevo evento</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/evento/allevent.php" class="nav-link">
                  <i class="fas fa-book nav-icon"></i>
                  <p>Todos Los Eventos</p>
                </a>
              </li>

            </ul>
          </li>
          
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-trophy"></i>
              <p>
                Premios
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="pages/premios/newpremio.php" class="nav-link">
                  <i class="fas fa-edit nav-icon"></i>
                  <p>Nuevos premios</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="pages/premios/allpremio.php" class="nav-link">
                  <i class="fas fa-book nav-icon"></i>
                  <p>Todos Los Premios</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="../premios/asigpremio.php" class="nav-link">
                  <i class="fas fa-book nav-icon"></i>
                  <p>Asignar Premio</p>
                </a>
              </li>
            </ul>
          <li class="nav-item has-treeview menu-open">
            <a href="pages/puntos/agrepuntos.php" class="nav-link ">
              <i class="nav-icon fas fa-sort-amount-up"></i>
              <p>
                Agregar Puntos
               </p>
            </a>
          </li>
        
      </nav>
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Panel de Control</h1>
          </div><!-- /.col -->
        </div>
      </div>
    </div>

    <section class="content">
      <div class="container-fluid">
        <form name="estEvento" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
        <div class="row">
          
          <div class="col-md-4">
            
            <div class="input-group-append">
            <label>Evento</label>
            </div>
            <div class="input-group md-4">
                <select class="form-control" name="lstEvento" id="lstEvento">
                          <option value="">Sin seleccionar</option>
                          <?php
                              $objUtilidades->llenar_combo("evento","id_evento,nombre","id_evento",$idconexion);
                           ?>
                </select>
            </div>
          </div>
          <div class="col-md-2 pt-2">
                <button type="submit" name="btnVer" id="btnVer" value="" class="btn btn-default float-left mt-4"><i class="fas fa-search"></i> Buscar</button>
          </div>
          
          
        </div>
        </form>

        <?php 
        if(isset($_REQUEST["btnVer"])){
            $vridEvento=        htmlspecialchars($_REQUEST["lstEvento"]);
            $crudVer = new Crud();
            $crudVer->tablas = "registrar_reciclaje";
            $crudVer->expresion ="id_evento,id_usuario,cantidad";
            $crudVer->condicion ="id_evento = '$vridEvento ' ";
            $contEvento=$crudVer->read();
            $arreEstEvento=$crudVer->filas;

            $objCrud            = new Crud();
            $objCrud->tablas        = "evento";
            $objCrud->expresion     = "id_evento,nombre";
            $objCrud->condicion = "id_evento = '$vridEvento ' ";
            $vrleerEvento          = $objCrud->read();
            $arregloEvento = $objCrud->filas;
            $nomEvento    =$arregloEvento[0]['nombre'];

            $cantidades=0;
            $sumaUsu=0;

            $arregloRep=array();

            $ban=0;

            foreach ($arreEstEvento as $valor) {

              # code...
              $vrSumausu              =$valor["id_usuario"];
              $vrSumacan              =$valor["cantidad"];

              $cantidades=$cantidades+$vrSumacan;

              if(in_array($vrSumausu, $arregloRep)){ $ban=$ban-1;}
              else{
                $arregloRep[$ban]=$vrSumausu;
                $sumaUsu ++;
              }

            }

         ?>
        <div class="row">
        <div class="col-md-12">
          <h3 class="text-dark m-2"><?php echo $nomEvento; ?></h3>
        </div>
          <div class="col-md-3">
            <div class="card card-primary card-outline">
              <div class="card-body box-profile" style="padding: 0px;">
                <div class="text-center" style="background: url();">
                  <?php echo "<a href='evento.php?id_evento=$vridEvento' >
                  <img class='profile-user-img img-fluid img-circle'
                       src='dist/img/sena-usua.png'
                       alt='User profile picture' style='margin-bottom: 40px;margin-top: 40px'></a>";?>
                </div>
                <h2 class="profile-username text-center"> Numero de Participantes </h2>
                <h6 class="text-center"><?php echo "N° : ".$sumaUsu; ?></h6>
              </div>
            </div>
          </div> 
          <div class="col-md-3">
            <div class="card card-primary card-outline">
              <div class="card-body box-profile" style="padding: 0px;">
                <div class="text-center" style="background: url();">
                  <img class="profile-user-img img-fluid img-circle"
                       src="dist/img/botella.png"
                       alt="User profile picture" style="margin-bottom: 40px;margin-top: 40px">
                </div>

                <h2 class="profile-username text-center"> Numero de Botellas </h2>
                <h3 class="profile-username text-center"> Recolectadas </h3>
                <h6 class="text-center"><?php echo "N° : ".$cantidades; ?></h6>
              </div>
            </div>
          </div> 
        </div>
      <?php } ?>
      </div>
    </section>
    
  </div>
  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>
<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE -->
<script src="dist/js/adminlte.js"></script>
<!-- OPTIONAL SCRIPTS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<script src="dist/js/demo.js"></script>
<script src="dist/js/pages/dashboard3.js"></script>
</body>
</html>
<?php 
	include_once("php/libreria.php");
    if (isset($_GET['id_evento'])) { 
    $id_busqueda    = $_GET['id_evento'];
    $objCrud 			=new crud();
    $objCrud->tablas  = "evento";
    $objCrud->expresion = "*";
    $objCrud->condicion = "id_evento = '$id_busqueda'";
    $objCrud->read();
    $ardatos      = $objCrud->filas;

            /*echo "<pre>";
                print_r($ardatos);
              echo "</pre>";*/



    $id         = $ardatos[0]['id_evento'];
    $idUsuario       = $ardatos[0]['id_usuario'];
    $idTipoR      = $ardatos[0]['id_tipo_reciclaje'];
    $nombre       = $ardatos[0]['nombre'];
    $lugar     = $ardatos[0]['lugar'];
    $h_inicio       = $ardatos[0]['h_inicio'];
    $h_final       = $ardatos[0]['h_final'];
    $f_inicio       = $ardatos[0]['fecha_i'];
    $f_final       = $ardatos[0]['fecha_f'];
    $observaciones       = $ardatos[0]['observaciones'];
    $path ='pages/evento/files/img/'.$id;
    $arregloEvento = $objCrud->filas;

  $objCrudPuntos    =new crud();
  $objCrudPuntos->tablas    ="registrar_reciclaje";
  $objCrudPuntos->expresion    ="*";
  $objCrudPuntos->condicion 	="id_evento= '$id_busqueda'";
  $objCrudPuntos->ordenamiento ="puntos_totales DESC, id_usuario ASC";
  $vrleerRanking               =$objCrudPuntos->read();
  /*if($vrleerRanking==0){

      echo "No hay ranking para mostrar <br>";
      die();}*/
  $arregloPuntos = $objCrudPuntos->filas;

   ?>

<?php include('header.php') ?>
				<!-- Main -->
					<div id="main">

						<!-- Post -->
							<article class="post">
								<header>
									<div class="title">
										<h2><?php echo $nombre; ?></h2>
										<p>Lugar del evento: <?php echo $lugar; ?></p>
									</div>
									<div class="meta">
										<time class="published" datetime="2015-11-01"><?php echo $f_inicio; ?></time>
										<h4>Fecha de inicio</h4>
									</div>
								</header>
								<?php
								if(file_exists($path)){
	                            $directorio = opendir($path);
	                            while($archivo= readdir($directorio)){
	                              if(!is_dir($archivo)){
	                                echo "<span class='image featured'><img src='pages/evento/files/img/$id/$archivo''  /></span>" ;
	                                  
	                                

	                             	 }
	                            	}
                         		 } ?>
								
								<?php echo $observaciones; ?>
								<footer>
									<ul class="stats">
										<li><a href="#">Fecha de Culminación</a></li>
										<li><time class="published" datetime="2020-02-07"><?php echo $f_final; ?></time></li>
									</ul>
								</footer>
							</article>

	<?php 
          
          } 
        ?>

							<article class="post">

															<section>
								<ul class="posts">
									<?php 
										$vrno =1;
										$ban=0;
										$arregloRep=array();
										foreach ($arregloPuntos as $valor1) {
											# code...
											$idEvento              =$valor1["id_evento"];
											$idUsuario             =$valor1["id_usuario"];
											$puntosT		       =$valor1["puntos_totales"];
											$objEve              = new crud();
										      $objEve->tablas      ="evento";
										      $objEve->expresion   ="*";
										      $objEve->condicion   ="id_evento='$idEvento'";
										      $eve                 =$objEve->read();
										      $eveArreglo          =$objEve->filas;
										      $nomEve		       =$eveArreglo[0]["nombre"];
										    $objUsu             = new crud();
											   $objUsu->tablas     ="usuario";
											   $objUsu->expresion  ="*";
											   $objUsu->condicion  ="id_usuario='$idUsuario'";
											   $vrcanusuarios      = $objUsu->read();
											   $usuArreglo       =$objUsu->filas;
											   $vrnombreusu         =$usuArreglo[0]["nombre"];
											   $vrapeusu			=$usuArreglo[0]["apellido"];
											   $vrEstaUsu			=$usuArreglo[0]["genero"];

										$ban=$ban+1;

										if(in_array($idUsuario, $arregloRep)){ $ban=$ban-1;}
										else{

										 ?>
										
									<li>
										<article>
											<header>
												<h3><?php echo $vrnombreusu." ".$vrapeusu; ?></h3>
												<p class="sinespa"><?php echo $puntosT; ?></p>
												<h3 class=""><?php echo "Posición ".$ban; ?></h3>
											</header>
											<?php 
											if($vrEstaUsu  =='M'){ ?>
											<img class="image" src="dist/img/avat02.jpg" alt="" />
											<?php 
											}else{?>
											<img class="image" src="dist/img/avat01.jpg" alt="" />
											<?php 
											}
											 ?>
											
										</article>
									</li>
								<?php
								$arregloRep[$ban-1]=$idUsuario;
								 }


								} ?>
									
								</ul>
							</section>

							</article>


				<!-- Footer -->
					<section id="footer" style="text-align: center;">
						<ul class="icons">
							<li><a href="https://twitter.com/SistemaRecolec1?s=08" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
							<li><a href="https://www.facebook.com/Sistema-Recolector-de-Materiales-Reciclables-105152521107868/" class="icon brands fa-facebook-f"><span class="label">Facebook</span></a></li>
							<li><a href="https://www.instagram.com/sistemarecolector/?hl=es-la" class="icon brands fa-instagram"><span class="label">Instagram</span></a></li>
							
						</ul>
					</section>

					</div>

			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>
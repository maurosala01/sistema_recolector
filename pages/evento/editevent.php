<?php 
session_start();
  $NombreSesion =  $_SESSION["nombre"]  ;
  $ApellidoSesion =  $_SESSION["apelido"] ;
  $PerfilSesion =  $_SESSION["id_perfil"] ;
  $idSesion =  $_SESSION["id_usuario"] ;
include_once("../../php/libreria.php");
if(empty($_SESSION["id_usuario"])){
  header("refresh:0; url=../form/login.html");
}
  $objconexion  = new Conexion();
  $idconexion   = $objconexion->conectar();
  $objCrud    = new Crud();
  $objUtilidades = new Utilidades();
  $objCrudSesion = new Crud();
  $objCrudSesion->tablas = "perfil_usuario";
  $objCrudSesion->expresion ="*";
  $objCrudSesion->condicion ="id_perfil = '$PerfilSesion ' ";
  $contar=$objCrudSesion->read();
  $arreglo=$objCrudSesion->filas;
  $nomPerfil=$arreglo[0]["perfil"];

 ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Editar Evento</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    <!-- summernote -->
  <link rel="stylesheet" href="../../plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->
      <?php 
if($PerfilSesion==1){
include ("../../include/navAdmin.php"); 
}else{
  include ("../../include/navOperario.php");
}
?>
  <?php 
    if (isset($_GET['id_evento'])) { 
    $id_busqueda    = $_GET['id_evento'];
    $objCrud->tablas  = "evento";
    $objCrud->expresion = "*";
    $objCrud->condicion = "id_evento = '$id_busqueda'";
    $objCrud->read();
    $ardatos      = $objCrud->filas;

            /*echo "<pre>";
                print_r($ardatos);
              echo "</pre>";*/



    $id         = $ardatos[0]['id_evento'];
    $idUsuario       = $ardatos[0]['id_usuario'];
    $idTipoR      = $ardatos[0]['id_tipo_reciclaje'];
    $nombre       = $ardatos[0]['nombre'];
    $lugar     = $ardatos[0]['lugar'];
    $h_inicio       = $ardatos[0]['h_inicio'];
    $h_final       = $ardatos[0]['h_final'];
    $f_inicio       = $ardatos[0]['fecha_i'];
    $f_final       = $ardatos[0]['fecha_f'];
    $observaciones       = $ardatos[0]['observaciones'];

   ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Editar Evento</h1>
          </div>
        </div>
        <?php if(isset($_SESSION['message'])) {?>

        <div class="alert alert-<?=$_SESSION['message_type'];?> alert-dismissible fade show" role="alert">    <?= $_SESSION['message']?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

      <?php $_SESSION['message'] = null; }?>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <form action="editevent.php?id_evento=<?php echo $_GET['id_evento']; ?>" enctype="multipart/form-data"S method="POST">
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- /.col -->
          <div class="col-md-9">
             <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">Evento</h3>
                <button type="submit" id="btnActualizar" name="btnActualizar" class="btn btn-default float-right"><i class="fas fa-pencil-alt"></i> Actualizar</button>


              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="form-group">
                  <label>Título</label>
                  <input class="form-control" name="eve_nombre" value="<?php echo $nombre; ?>" placeholder="Titulo:">
                </div>
                <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Lugar</label>
                          <input class="form-control" name="eve_lugar" value="<?php echo $lugar; ?>" placeholder="Lugar:">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Tipo de Reciclaje</label>
                          <select class="form-control" name="eve_tipoR" id="eve_tipoR">
                            <option ><?php echo $idTipoR; ?></option>
                            <?php
                                $objUtilidades->llenar_combo("tipo_reciclaje","id_tipo_reciclaje,nombre","id_tipo_reciclaje",$idconexion);
                             ?> 
                         </select>
                      </div>
                    </div>
                </div>
                </div>
                <div class="form-group">
                    <div class="row">
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Fecha de Inicio</label>
                          <input class="form-control" name="eve_fi" id="eve_fi" value= "<?php echo $f_inicio ?>" type="date">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Fecha de fin</label>
                          <input class="form-control"  name="eve_ff" value= "<?php echo $f_final ?>" type="date">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Hora Inicio</label>
                          <input class="form-control" name="eve_hi" value= "<?php echo $h_inicio ?>" type="time">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>Hora Fin</label>
                          <input class="form-control" name="eve_hf" value= "<?php echo $h_final ?>" type="time">
                      </div>
                    </div>
                </div>
                </div>

                <div class="form-group">
                    <input class="form-control" name="eve_desc" value="<?php echo $observaciones ?>" placeholder="Descripción...">

                <div class="col-md-8">
                        <div class="form-group">
                          <label for="archivo" class="pt-4">Subir imagén</label>
                          <input  type="file" class="form-control" id="archivo" name="archivo" accept="image/*">
                          <?php 
                          $path ='./files/img/'.$id;
                          if(file_exists($path)){
                            $directorio = opendir($path);
                            while($archivo= readdir($directorio)){
                              if(!is_dir($archivo)){
                                echo "<div data='".$path."/".$archivo."><a href='".$path."/".$archivo."' title ='ver archivo adjunto'><spanclass='glyphicon glyphicon-picture'></span></a>";
                                echo "$archivo <a href='#' class='delete' title='ver archivo adjunto'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></a></div>";
                                echo "<img src='files/img/$id/$archivo' width='300'/>";
                                  
                                

                              }
                            }
                          }
                          ?>
                        </div>
                      </div>


                </div>
              </div>
              <!-- /.card-body -->

            </div>

            </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      <!-- /.container-fluid -->
    </section>
    </form>
    <?php   

      //Llamar al metodo update para guardar /modificar datos

      if (isset($_POST["btnActualizar"])) {

        $vridEve=$id;

        $vrevenombre  = htmlspecialchars($_POST["eve_nombre"]);
        $vrevelugar = htmlspecialchars($_POST["eve_lugar"]);
        $vrevetipoR  = htmlspecialchars($_POST["eve_tipoR"]);
        $vreveobservaciones  = htmlspecialchars($_POST["eve_desc"]);

        $vreveff  = htmlspecialchars($_POST["eve_ff"]);
        $vrevehi  = htmlspecialchars($_POST["eve_hi"]);
        $vrevehf  = htmlspecialchars($_POST["eve_hf"]);
        $vrevefi  = htmlspecialchars($_POST["eve_fi"]);
        //crgar imagen
      if($_FILES["archivo"]["error"]>0){
      }
      else{
        $carpeta ='./files/img/'.$vridEve;
            foreach (glob($carpeta."/*") as $archivos_carpeta) {
                # code...
                if(is_dir($archivos_carpeta)){
                    eliminarDir($archivos_carpeta);
                }
                else{
                    unlink($archivos_carpeta);
                }
            }
          
        $permitido= array("image/jpg","image/png","image/jpeg");
        if(in_array($_FILES["archivo"]["type"], $permitido)){
          $ruta= './files/img/'.$vridEve.'/';
          $arch=$ruta.$_FILES["archivo"]["name"];

          if(!file_exists($ruta)){
            mkdir($ruta, 0777, true);
          }

          if(!file_exists($arch)){

            $resultado= @move_uploaded_file($_FILES["archivo"]["tmp_name"], $arch);

          }
          else{
            $_SESSION['message'] = 'archivo ya existe';
            $_SESSION['message_type'] = 'secondary';
          }

        }
        else{
          $_SESSION['message'] = 'archivo no permitido';
          $_SESSION['message_type'] = 'secondary';
        }
      }

        $objCrud = new Crud();
        $objCrud ->tablas     = "evento";
        $objCrud ->expresion  = "id_tipo_reciclaje = '$vrevetipoR', nombre = '$vrevenombre', lugar = '$vrevelugar', h_inicio = '$vrevehi', h_final = '$vrevehf', fecha_i = '$vrevefi', fecha_f = '$vreveff', observaciones ='$vreveobservaciones'";
        //$objCrud ->expresion ="'Doc_id'";
        
        $objCrud ->condicion = "id_evento='$vridEve'";
        $objCrud ->update();

        
        $_SESSION['message'] = 'Evento actualizado!!!';
        $_SESSION['message_type'] = 'PRIMARY';
        //include ("alluser.php");
        //header("Location:allevent.php");
        echo "<a href='allevent.php' >Verificar</a>";


      }


    ?>

    <!-- /.content -->
  </div>
  </div>
  <?php 
          
          } 
        ?>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<!-- Summernote -->
<script src="../../plugins/summernote/summernote-bs4.min.js"></script>
<!-- Page Script -->
<script>
  $(function () {
    //Add text editor
    $('#compose-textarea').summernote()
  })
</script>
</body>
</html>

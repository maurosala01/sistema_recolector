<?php 
session_start();
  $NombreSesion =  $_SESSION["nombre"]  ;
  $ApellidoSesion =  $_SESSION["apelido"] ;
  $PerfilSesion =  $_SESSION["id_perfil"] ;
  $idSesion =  $_SESSION["id_usuario"] ;
  include_once("../../php/libreria.php");
  if(empty($_SESSION["id_usuario"])){
  header("refresh:0; url=../form/login.html");
}
  $objCrudSesion = new Crud();
  $objCrudSesion->tablas = "perfil_usuario";
  $objCrudSesion->expresion ="*";
  $objCrudSesion->condicion ="id_perfil = '$PerfilSesion ' ";
  $contar=$objCrudSesion->read();
  $arreglo=$objCrudSesion->filas;
  $nomPerfil=$arreglo[0]["perfil"];

  $objConexion   = new Conexion(); 
  $idconexion    = $objConexion->conectar();

  $objUtilidades = new Utilidades();
  $vridpremio = $objUtilidades->consecutivo("premio","id_premio",$idconexion);
  $vrPevento= "";
  $vrPdescripcion= "";
  if(isset($_REQUEST["btnAuxiliar"])){
      $vrPdescripcion=        htmlspecialchars($_REQUEST["txtPdescripcion"]);
      $vrPevento=   htmlspecialchars($_REQUEST["lstEvento"]);

      $objCrud          = new Crud();
      $objCrud->tablas  = "premio";
      $objCrud->campos  = "id_premio, id_evento, descripcion_premio";
      $objCrud->valores ="'$vridpremio','$vrPevento','$vrPdescripcion'";
      $objCrud->create($idconexion);
      $_SESSION['message'] = 'Premio Registrado!!!';
      $_SESSION['message_type'] = 'success';
    }
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Nuevo Premio</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php 
if($PerfilSesion==1){
include ("../../include/navAdmin.php"); 
}else{
  include ("../../include/navOperario.php");
}
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Nuevo Premio</h1>
          </div>
        </div>
         <?php if(isset($_SESSION['message'])) {?>

        <div class="alert alert-<?=$_SESSION['message_type'];?> alert-dismissible fade show" role="alert">    <?= $_SESSION['message']?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

      <?php $_SESSION['message'] = null; }?>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <form name="frmIngPremio" id="frmIngPremio" method="post"  action='<?php echo $_SERVER["PHP_SELF"]; ?>'>
      <div class="container-fluid">
        <div class="row">
          
          <div class="col-md-4">
            <div class="input-group-append">
            <label>Premio</label>
            </div>
            <div class="input-group mb-3">
              <input type="text" class="form-control" name="txtPdescripcion" placeholder="Premio">
              <div class="input-group-append">
                <div class="input-group-text">
                  <span class="fas fa-wine-bottle"></span>
                </div>
              </div>
            </div>
            <div class="input-group-append">
            <label>Evento</label>
            </div>
            <div class="input-group mb-3">
                <select class="form-control" name="lstEvento" id="lstEvento">
                          <option value="">Sin seleccionar</option>
                          <?php
                              $objUtilidades->llenar_combo("evento","id_evento,nombre","id_evento",$idconexion);
                           ?>

                </select>
            </div>
            <div class="input-group mb-3">
              <div class="form-group">
                <button type="submit" name="btnAuxiliar" id="btnAuxiliar" value="Registrar" class="btn btn-default float-right"><i class="fas fa-pencil-alt"> Regisrar</i> </button>
              </div>
           </div>
          </div>
          


          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
      </form>
    </section>
    <!-- /.content -->
  </div>
   <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>


<a href="../../php/salir.php" class="btn btn-default btn-sm" title="salir" style="position: absolute;right: 0px">
   <i class="fas fa-power-off"></i>
  </a>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../admin.php" class="brand-link">
      <img src="../../dist/img/AdminLTELogo.png"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Panel de Control</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

      <!-- Sidebar Menu -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
        <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item has-treeview ">
              <a href="" class="nav-link pl-0 pb-0">
                  <h5>
                  <?php echo $NombreSesion." ".$ApellidoSesion; ?>
                  </h5>
                </a>
                <ul class="nav nav-treeview">
                 <li class="nav-item">
                    <a href="../usuarios/editperfil.php" class="nav-link">
                      <i class="fas fa-edit nav-icon"></i>
                    <p>Editar</p>
                   </a>
                 </li>
               </ul>
             </li>
            </ul>
          </nav>
          <a><?php echo $nomPerfil ?></a>
        </div>
      </div>
<nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="../../admin.php" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Escritorio
               </p>
            </a>
          </li>
          <li class="nav-item ">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-user-friends"></i>
              <p>
                Usuarios
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../usuarios/newuser.php" class="nav-link">
                  <i class="nav-icon fas fa-user-check"></i>
                  <p>Nuevo Usuario</p>
                </a>
  
              </li>
              <li class="nav-item">
                <a href="../usuarios/alluser.php" class="nav-link">
                  <i class="nav-icon fas fa-users"></i>
                  <p>Todos Los Usuarios</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Eventos
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../evento/newevent.php" class="nav-link">
                  <i class="fas fa-edit nav-icon"></i>
                  <p>Nuevo evento</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../evento/allevent.php" class="nav-link">
                  <i class="fas fa-book nav-icon"></i>
                  <p>Todos Los Eventos</p>
                </a>
              </li>
              
            </ul>
          </li>
          <li class="nav-item ">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-recycle"></i>
              <p>
                Tipo de Reciclaje
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../TipoReciclaje/newTipoR.php" class="nav-link">
                  <i class="nav-icon fas fa-edit"></i>
                  <p>Nuevo reciclaje</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../TipoReciclaje/allTipoR.php" class="nav-link">
                  <i class="nav-icon fas fa-book"></i>
                  <p>Lista reciclaje</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-trophy"></i>
              <p>
                Premios
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="../premios/newpremio.php" class="nav-link">
                  <i class="fas fa-edit nav-icon"></i>
                  <p>Nuevos premios</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../premios/allpremio.php" class="nav-link">
                  <i class="fab fa-audible nav-icon"></i>
                  <p>Todos Los Premios</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="../premios/asigpremio.php" class="nav-link">
                  <i class="fas fa-book nav-icon"></i>
                  <p>Asignar Premio</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link ">
              <i class="nav-icon fas fa-sort-amount-up"></i>
              <p>
                Puntos
                <i class="right fas fa-angle-left"></i>
               </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item has-treeview menu-open">
               <a href="../puntos/agrepuntos.php" class="nav-link ">
                <i class="nav-icon fas fa-award"></i>
                <p>
                 Agregar Puntos
                </p>
              </a>
             </li>
              <li class="nav-item has-treeview menu-open">
               <a href="../puntos/regpremio.php" class="nav-link ">
                <i class="nav-icon fas fa-bezier-curve"></i>
                <p>
                 Registro de premios
                </p>
              </a>
             </li>
              <li class="nav-item has-treeview menu-open">
               <a href="../puntos/regreciclaje.php" class="nav-link ">
                <i class="nav-icon fab fa-buffer"></i>
                <p>
                 Registro de Reciclaje
                </p>
              </a>
             </li>             
            </ul>
          </li>
        
      </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
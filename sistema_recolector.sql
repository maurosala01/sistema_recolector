-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-03-2020 a las 23:39:26
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sistema_recolector`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bonos`
--

CREATE TABLE `bonos` (
  `id_bono` int(11) NOT NULL,
  `id_evento` int(11) DEFAULT NULL,
  `descripcion` varchar(120) COLLATE utf8_spanish_ci NOT NULL,
  `valor_puntos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evento`
--

CREATE TABLE `evento` (
  `id_evento` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_tipo_reciclaje` int(11) DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `lugar` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `h_inicio` time NOT NULL,
  `h_final` time NOT NULL,
  `fecha_i` date NOT NULL,
  `fecha_f` date NOT NULL,
  `observaciones` varchar(1000) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `evento`
--

INSERT INTO `evento` (`id_evento`, `id_usuario`, `id_tipo_reciclaje`, `nombre`, `lugar`, `h_inicio`, `h_final`, `fecha_i`, `fecha_f`, `observaciones`) VALUES
(1, 1, 1, 'PRIMER RECICLATON SENA', 'SENA CTPI sede Norte', '08:00:00', '13:00:00', '2020-03-05', '2020-03-07', 'lorem euismod placerat. Vivamus porttitor magna enim, ac accumsan tortor cursus at. Phasellus sed ultricies mi non congue ullam corper. Praesent tincidunt sed tellus ut rutrum. Sed vitae justo condimentum, porta lectus vitae, ultricies congue gravida diam non fringilla.'),
(2, 1, 1, 'RECICLA POR UN FUTURO MEJOR', 'SENA CTPI sede Norte', '09:00:00', '12:00:00', '2020-03-09', '2020-03-14', 'Gracias al reciclaje se previene el desuso de materiales potencialmente útiles, se reduce el consumo de nueva materia prima, además de reducir el uso de energía, la contaminación del aire (a través de la incineración) y del agua (a través de los vertederos), así como también disminuir las emisiones de gases de efecto invernadero en comparación con la producción de plásticos.  El reciclaje es un componente clave en la reducción de desechos contemporáneos y es el tercer componente de las 3R («Reducir, Reutilizar y Reciclar»).  Los materiales reciclables son muchos, e incluyen todo el papel y cartón, el vidrio, los metales ferrosos y no ferrosos, algunos plásticos, telas y textiles, maderas y componentes electrónicos. En otros casos no es posible llevar a cabo un reciclaje debido a la dificultad técnica o alto coste del proceso, de modo que suele reutilizarse el material o los productos para producir otros materiales y se destinan a otras finalidades, como el aprovechamiento energético.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulo`
--

CREATE TABLE `modulo` (
  `id_modulo` int(11) NOT NULL,
  `nombre` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulo_perfil`
--

CREATE TABLE `modulo_perfil` (
  `id_modulo_perfil` int(11) NOT NULL,
  `id_perfil` int(11) DEFAULT NULL,
  `id_modulo` int(11) DEFAULT NULL,
  `permiso` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfil_usuario`
--

CREATE TABLE `perfil_usuario` (
  `id_perfil` int(11) NOT NULL,
  `perfil` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `perfil_usuario`
--

INSERT INTO `perfil_usuario` (`id_perfil`, `perfil`) VALUES
(1, 'Administrador'),
(2, 'Operario'),
(3, 'Usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `premio`
--

CREATE TABLE `premio` (
  `id_premio` int(11) NOT NULL,
  `id_evento` int(11) DEFAULT NULL,
  `descripcion_premio` text COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `premio`
--

INSERT INTO `premio` (`id_premio`, `id_evento`, `descripcion_premio`) VALUES
(1, 2, 'premio1'),
(2, 1, 'premio2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registrar_premio`
--

CREATE TABLE `registrar_premio` (
  `id_ranking` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_premio` int(11) DEFAULT NULL,
  `puntaje_total` int(11) NOT NULL,
  `posicion` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registrar_reciclaje`
--

CREATE TABLE `registrar_reciclaje` (
  `id_reciclaje` int(11) NOT NULL,
  `id_evento` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `cantidad` int(11) NOT NULL,
  `puntos` int(11) NOT NULL,
  `puntos_bono` int(11) NOT NULL,
  `puntos_totales` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `registrar_reciclaje`
--

INSERT INTO `registrar_reciclaje` (`id_reciclaje`, `id_evento`, `id_usuario`, `cantidad`, `puntos`, `puntos_bono`, `puntos_totales`) VALUES
(1, 1, 36, 10, 10, 0, 10),
(2, 1, 37, 20, 20, 0, 20),
(3, 1, 38, 15, 15, 0, 15),
(4, 1, 39, 23, 23, 0, 23),
(5, 1, 40, 14, 14, 0, 14),
(6, 2, 41, 30, 30, 0, 30),
(7, 2, 42, 23, 23, 0, 23),
(8, 2, 43, 15, 15, 0, 15),
(9, 2, 44, 24, 24, 0, 24),
(10, 2, 45, 26, 26, 0, 26);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registro_evento`
--

CREATE TABLE `registro_evento` (
  `id_registro_evento` int(11) NOT NULL,
  `id_evento` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_reciclaje`
--

CREATE TABLE `tipo_reciclaje` (
  `id_tipo_reciclaje` int(11) NOT NULL,
  `nombre` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `caracteristicas` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `puntos_botella` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tipo_reciclaje`
--

INSERT INTO `tipo_reciclaje` (`id_tipo_reciclaje`, `nombre`, `caracteristicas`, `puntos_botella`) VALUES
(1, 'plastico', 'botella', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `id_perfil` int(11) DEFAULT NULL,
  `documento` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `contrasena` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `id_perfil`, `documento`, `telefono`, `nombre`, `apellido`, `correo`, `contrasena`, `estado`) VALUES
(1, 1, '1750239', '3104572291', 'Usuario', 'Admin', 'sistemarecolector@gmail.com', 'dbb2d77bd76fe13757f45c509cc3b27b', 1),
(2, 2, '1061748368', '3104572291', 'James', 'Gomez', 'jmgomez863@misena.edu.co', '14af2b7928556199ab5d4364fd221b12', 1),
(3, 2, '1063810896', '3225132338', 'Fabian', 'Agredo', 'fmagredo6@misena.edu.co', 'e10adc3949ba59abbe56e057f20f883e', 1),
(4, 2, '1061759168', '3233489160', 'Yordi', 'Velasco', 'ysvelasco8@misena.edu.co', 'e10adc3949ba59abbe56e057f20f883e', 1),
(5, 2, '1061687559', '3117030041', 'Jaime', 'Paz', 'jfpaz95@misena.edu.co', '21a18a24357fb1b960e09e276abd8e90', 1),
(6, 2, '1144032464', '3172427574', 'Cristhian', 'Goyez', 'cagoyez@misena.edu.co', '422e73e464dedeac95a477ae277bdf38', 1),
(7, 2, '1036621159', '3122614161', 'Alexander', 'Gonzalez', 'ragonzalez951@misena.edu.co', 'b719d8961fb6b56f1af56125974f0114', 1),
(8, 2, '10295871', '3147991931', 'Juan', 'Samboni', 'jcsamboni17@misena.edu.co', 'e10adc3949ba59abbe56e057f20f883e', 1),
(9, 2, '10303845', '3116137519', 'Cristian', 'Garcia', 'crisyan1906@misena.edu.co', 'b87415be3661f7d9baece6121a8d4504', 1),
(36, 3, '1061773804', '0', 'ANDRES FELIPE ', 'BUITRON NARVAEZ', 'prueba@misena.edu.co', '12345', 1),
(37, 3, '1061783735', '0', 'ANDRES FELIPE ', 'RUIZ PEÑA', 'prueba@misena.edu.co', '12345', 1),
(38, 3, '1002956506', '0', 'CHELLSYN ARIANA ', 'ARBOLEDA ZAMBRANO', 'prueba@misena.edu.co', '12345', 1),
(39, 3, '1002777359', '0', 'CRISAN DAVID ', 'LOPEZ MOSQUERA', 'prueba@misena.edu.co', '12345', 1),
(40, 3, '1002971410', '0', 'DANIEL ESVEN ', 'IMBACHI HOYOS', 'prueba@misena.edu.co', '12345', 1),
(41, 3, '1061760421', '0', 'DIEGO FERNANDO ', 'DORADO PISMAG', 'prueba@misena.edu.co', '12345', 1),
(42, 3, '1002953430', '0', 'DORIS ADRIANA ', 'ZAMBRANO HUILA', 'prueba@misena.edu.co', '12345', 1),
(43, 3, '1002961356', '0', 'ERICK ', 'SANDOVAL RESTREPO', 'prueba@misena.edu.co', '12345', 1),
(44, 3, '1061753872', '0', 'GERSON ', 'SANDOVAL OTERO', 'prueba@misena.edu.co', '12345', 1),
(45, 3, '1002965852', '0', 'JAIDER STEVEN ', 'MAZABUEL MUÑOZ', 'prueba@misena.edu.co', '12345', 1),
(46, 3, '1061737581', '0', 'JENHIFFER ALEJANDRA ', 'HOYOS ALEGRIA', 'prueba@misena.edu.co', '12345', 1),
(47, 3, '1193228555', '0', 'JHONIER ARTURO ', 'CORDOBA ALVAREZ', 'prueba@misena.edu.co', '12345', 1),
(48, 3, '1003150316', '0', 'JOHANA ANDREA ', 'DAGUA TOMBE', 'prueba@misena.edu.co', '12345', 1),
(49, 3, '1058669805', '0', 'JOSE ANDRUVAR ', 'BELTRAN GAVIRIA', 'prueba@misena.edu.co', '12345', 1),
(50, 3, '1061759568', '0', 'JUAN MANUEL ', 'PEREZ ACEVEDO', 'prueba@misena.edu.co', '12345', 1),
(51, 3, '1033785378', '0', 'JULIAN ANDRES ', 'BOLAÑOS ARCOS', 'prueba@misena.edu.co', '12345', 1),
(52, 3, '1061793409', '0', 'JULIO EDUARDO ', 'CAMPO UNI', 'prueba@misena.edu.co', '12345', 1),
(53, 3, '1004491191', '0', 'KAREN TAANA ', 'MARNEZ VALENCIA', 'prueba@misena.edu.co', '12345', 1),
(54, 3, '1002792138', '0', 'LEIDER YESIR ', 'JIMENEZ MUÑOZ', 'prueba@misena.edu.co', '12345', 1),
(55, 3, '1060240252', '0', 'MARCOS ROBINSON ', 'CHAPID CHAMORRO', 'prueba@misena.edu.co', '12345', 1),
(56, 3, '1061741774', '0', 'ROCIO ', 'CERON GALINDEZ', 'prueba@misena.edu.co', '12345', 1),
(57, 3, '1193522673', '0', 'ROCISELA ', 'PRIETO MUÑOZ', 'prueba@misena.edu.co', '12345', 1),
(58, 3, '1061719208', '0', 'RUBEN DARIO ', 'ZUÑIGA SANCHEZ', 'prueba@misena.edu.co', '12345', 1),
(59, 3, '1002955851', '0', 'SANAGO ', 'PRADO ASTAIZA', 'prueba@misena.edu.co', '12345', 1),
(60, 3, '1002961590', '0', 'VIANY JINETH ', 'CERON HORMIGA', 'prueba@misena.edu.co', '12345', 1),
(61, 3, '1003035765', '0', 'YEFERSON ANDRES ', 'MENDEZ GOMEZ', 'prueba@misena.edu.co', '12345', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bonos`
--
ALTER TABLE `bonos`
  ADD PRIMARY KEY (`id_bono`),
  ADD UNIQUE KEY `id_bono` (`id_bono`),
  ADD KEY `IX_Relationship16` (`id_evento`);

--
-- Indices de la tabla `evento`
--
ALTER TABLE `evento`
  ADD PRIMARY KEY (`id_evento`),
  ADD UNIQUE KEY `id_evento` (`id_evento`),
  ADD KEY `IX_Relationship6` (`id_usuario`),
  ADD KEY `IX_Relationship9` (`id_tipo_reciclaje`);

--
-- Indices de la tabla `modulo`
--
ALTER TABLE `modulo`
  ADD PRIMARY KEY (`id_modulo`),
  ADD UNIQUE KEY `id_modulo` (`id_modulo`);

--
-- Indices de la tabla `modulo_perfil`
--
ALTER TABLE `modulo_perfil`
  ADD PRIMARY KEY (`id_modulo_perfil`),
  ADD UNIQUE KEY `id_modulo_perfil` (`id_modulo_perfil`),
  ADD KEY `IX_Relationship4` (`id_perfil`),
  ADD KEY `IX_Relationship5` (`id_modulo`);

--
-- Indices de la tabla `perfil_usuario`
--
ALTER TABLE `perfil_usuario`
  ADD PRIMARY KEY (`id_perfil`),
  ADD UNIQUE KEY `id_perfil` (`id_perfil`);

--
-- Indices de la tabla `premio`
--
ALTER TABLE `premio`
  ADD PRIMARY KEY (`id_premio`),
  ADD UNIQUE KEY `id_premio` (`id_premio`),
  ADD KEY `IX_Relationship18` (`id_evento`);

--
-- Indices de la tabla `registrar_premio`
--
ALTER TABLE `registrar_premio`
  ADD PRIMARY KEY (`id_ranking`),
  ADD UNIQUE KEY `id_ranking` (`id_ranking`),
  ADD KEY `IX_Relationship13` (`id_usuario`),
  ADD KEY `IX_Relationship19` (`id_premio`);

--
-- Indices de la tabla `registrar_reciclaje`
--
ALTER TABLE `registrar_reciclaje`
  ADD PRIMARY KEY (`id_reciclaje`),
  ADD UNIQUE KEY `id_reciclaje` (`id_reciclaje`),
  ADD KEY `IX_Relationship8` (`id_evento`),
  ADD KEY `IX_Relationship12` (`id_usuario`);

--
-- Indices de la tabla `registro_evento`
--
ALTER TABLE `registro_evento`
  ADD PRIMARY KEY (`id_registro_evento`),
  ADD UNIQUE KEY `id_registro_evento` (`id_registro_evento`),
  ADD KEY `IX_Relationship7` (`id_evento`),
  ADD KEY `IX_Relationship11` (`id_usuario`);

--
-- Indices de la tabla `tipo_reciclaje`
--
ALTER TABLE `tipo_reciclaje`
  ADD PRIMARY KEY (`id_tipo_reciclaje`),
  ADD UNIQUE KEY `id_tipo_reciclaje` (`id_tipo_reciclaje`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `id_usuario` (`id_usuario`),
  ADD KEY `IX_Relationship1` (`id_perfil`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bonos`
--
ALTER TABLE `bonos`
  MODIFY `id_bono` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `evento`
--
ALTER TABLE `evento`
  MODIFY `id_evento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `modulo`
--
ALTER TABLE `modulo`
  MODIFY `id_modulo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `modulo_perfil`
--
ALTER TABLE `modulo_perfil`
  MODIFY `id_modulo_perfil` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `perfil_usuario`
--
ALTER TABLE `perfil_usuario`
  MODIFY `id_perfil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `premio`
--
ALTER TABLE `premio`
  MODIFY `id_premio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `registrar_premio`
--
ALTER TABLE `registrar_premio`
  MODIFY `id_ranking` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `registrar_reciclaje`
--
ALTER TABLE `registrar_reciclaje`
  MODIFY `id_reciclaje` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `registro_evento`
--
ALTER TABLE `registro_evento`
  MODIFY `id_registro_evento` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tipo_reciclaje`
--
ALTER TABLE `tipo_reciclaje`
  MODIFY `id_tipo_reciclaje` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `bonos`
--
ALTER TABLE `bonos`
  ADD CONSTRAINT `Relationship16` FOREIGN KEY (`id_evento`) REFERENCES `evento` (`id_evento`);

--
-- Filtros para la tabla `evento`
--
ALTER TABLE `evento`
  ADD CONSTRAINT `Relationship6` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`),
  ADD CONSTRAINT `Relationship9` FOREIGN KEY (`id_tipo_reciclaje`) REFERENCES `tipo_reciclaje` (`id_tipo_reciclaje`);

--
-- Filtros para la tabla `modulo_perfil`
--
ALTER TABLE `modulo_perfil`
  ADD CONSTRAINT `Relationship4` FOREIGN KEY (`id_perfil`) REFERENCES `perfil_usuario` (`id_perfil`),
  ADD CONSTRAINT `Relationship5` FOREIGN KEY (`id_modulo`) REFERENCES `modulo` (`id_modulo`);

--
-- Filtros para la tabla `premio`
--
ALTER TABLE `premio`
  ADD CONSTRAINT `Relationship18` FOREIGN KEY (`id_evento`) REFERENCES `evento` (`id_evento`);

--
-- Filtros para la tabla `registrar_premio`
--
ALTER TABLE `registrar_premio`
  ADD CONSTRAINT `Relationship13` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`),
  ADD CONSTRAINT `Relationship19` FOREIGN KEY (`id_premio`) REFERENCES `premio` (`id_premio`);

--
-- Filtros para la tabla `registrar_reciclaje`
--
ALTER TABLE `registrar_reciclaje`
  ADD CONSTRAINT `Relationship12` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`),
  ADD CONSTRAINT `Relationship8` FOREIGN KEY (`id_evento`) REFERENCES `evento` (`id_evento`);

--
-- Filtros para la tabla `registro_evento`
--
ALTER TABLE `registro_evento`
  ADD CONSTRAINT `Relationship11` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id_usuario`),
  ADD CONSTRAINT `Relationship7` FOREIGN KEY (`id_evento`) REFERENCES `evento` (`id_evento`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `Relationship1` FOREIGN KEY (`id_perfil`) REFERENCES `perfil_usuario` (`id_perfil`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

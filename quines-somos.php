<?php include('header.php') ?>

<div id="main">

						<!-- Post -->
							<article class="post">
								<header>
							<div class="title">
										<h3>NUESTROS EVENTOS INSTITUCIONALES SON UNA PROPUESTA Y PRIORIDAD PARA LA GESTIÓN DE LOS RESIDUOS DE LA COMUNIDAD SENA</h3>
										<div style="background: url(dist/img/about-2.jpg);width: 100%;height: 400px;background-size: cover;background-repeat: no-repeat;background-position: center center;float: left;margin: 5px">
								</div>
							</div>
							<div>
							<p style="margin-top: 2em; text-transform: none; line-height: 2; text-align: justify; margin-right: 15px; font-size: 0.8em">
								En cada evento nos colocamos la camiseta por nuestro planeta incentivando a la comunidad SENA a crear una cultura ambiental teniendo en cuenta el valor y el manejo del material recicleble. Para ello expresamos en cada paso los ideales y expectativas	frente al tema recordando la importancia de cuidar el planeta Tierra.		
								
								<div style="background: url(dist/img/about.jpg);width: 45%;height: 300px;background-size: cover;background-repeat: no-repeat;background-position: center center;float: left;margin: 5px">
								</div>
								<div style="background: url(dist/img/about-3.jpg);width: 45%;height: 300px;background-size: cover;background-repeat: no-repeat;background-position: center center;float: left;margin: 5px">
								</div>

								<div style="margin: 10px">
									<h5 style="width: 100%">"Nosotros tenemos que ser el cambio que queremos ver en el mundo"</h5>
									<h6>— Mahatma Gandhi</h6>
								</div>

							</p>
							</div>
						</header>
						</article>
						<article class="post">
							<header>
								<div>
								<h4 style="margin-left: 2em;margin-top: 2em;font-size: 1em">MISIÓN</h4>
								<p style="margin-top: 2em; text-transform: none; line-height: 2; text-align: justify; margin-right: 2em; margin-left: 2em; font-size: 0.8em">El Sistema Recolector de Residuos Materiales tiene como misión promover la recolección y reciclaje de botellas plásticas dentro de las instalaciones SENA, creando las condiciones necesarias para impulsar y fortalecer la cultura del reciclaje dentro de la comunidad estudiantil.</p>
								</div>
								<div>
								<h4 style="margin-left: 2em;margin-top: 2em;font-size: 1em">VISIÓN</h4>
								<p style="margin-top: 2em; text-transform: none; line-height: 2; text-align: justify; margin-right: 2em; margin-left: 2em; font-size: 0.8em">Para el año 2022 el Sistema Recolector de Materiales Reciclables ampliará la cobertura del servicio en todas las sedes del SENA regional Cauca, además de ampliar la recolección de otros materiales como: papel y metales.</p>
								</div>
							</header>
						</article>



				<!-- Footer -->
					<section id="footer" style="text-align: center;">
						<ul class="icons">
							<li><a href="https://twitter.com/SistemaRecolec1?s=08" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
							<li><a href="https://www.facebook.com/Sistema-Recolector-de-Materiales-Reciclables-105152521107868/" class="icon brands fa-facebook-f"><span class="label">Facebook</span></a></li>
							<li><a href="https://www.instagram.com/sistemarecolector/?hl=es-la" class="icon brands fa-instagram"><span class="label">Instagram</span></a></li>
						</ul>
					</section>

					</div>


			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>
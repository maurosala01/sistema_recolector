<?php 
session_start();
  $NombreSesion =  $_SESSION["nombre"]  ;
  $ApellidoSesion =  $_SESSION["apelido"] ;
  $PerfilSesion =  $_SESSION["id_perfil"] ;
  $idSesion =  $_SESSION["id_usuario"] ;
include_once("../../php/libreria.php");
if(empty($_SESSION["id_usuario"])){
  header("refresh:0; url=../form/login.html");
}
  $objconexion  = new Conexion();
  $idconexion   = $objconexion->conectar();
  $objCrud    = new Crud();
  $objUtilidades = new Utilidades();
  $objCrudSesion = new Crud();
  $objCrudSesion->tablas = "perfil_usuario";
  $objCrudSesion->expresion ="*";
  $objCrudSesion->condicion ="id_perfil = '$PerfilSesion ' ";
  $contar=$objCrudSesion->read();
  $arreglo=$objCrudSesion->filas;
  $nomPerfil=$arreglo[0]["perfil"];

 ?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Editar Tipo de reciclaje</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
    <!-- Right navbar links -->
  </nav>
  <!-- /.navbar -->

        <?php 
if($PerfilSesion==1){
include ("../../include/navAdmin.php"); 
}else{
  include ("../../include/navOperario.php");
}
?>
<?php 
    if (isset($_GET['id_tipo_reciclaje'])) { 
    $id_busqueda    = $_GET['id_tipo_reciclaje'];
    $objCrud->tablas  = "tipo_reciclaje";
    $objCrud->expresion = "*";
    $objCrud->condicion = "id_tipo_reciclaje = '$id_busqueda'";
    $objCrud->read();
    $ardatos      = $objCrud->filas;

            /*echo "<pre>";
                print_r($ardatos);
              echo "</pre>";*/

    $id                  = $ardatos[0]['id_tipo_reciclaje'];
    $nombre              = $ardatos[0]['nombre'];
    $caracteristicas     = $ardatos[0]['caracteristicas'];
    $puntos              = $ardatos[0]['puntos_botella'];
   ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>EDITAR TIPO DE RECICLAJE</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <form action="editTipoR.php?id_tipo_reciclaje=<?php echo $_GET['id_tipo_reciclaje']; ?>" method="POST" >
    <section class="content">
      <div class="container-fluid">
          <div class="col-md-12">
             <div class="card card-primary card-outline">
              <!-- /.card-header -->
              <div class="card-body">
                <div class="form-group m-0">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                       	 <label>Nombre:</label>
                       	 <input class="form-control" name="TRnombre"
                       	 type="text" value="<?php echo $nombre; ?>">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                       	 <label>Características:</label>
                       	 <input class="form-control" name="TRcaracteristicas" value="<?php echo $caracteristicas; ?>" 
                       	 type="text">
                        </div>
                    </div>
                	</div>
                </div>
                <div class="form-group m-0">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                       	 <label>Puntos por botella:</label>
							<input class="form-control" type="number" name="TRpuntos" value="<?php echo $puntos; ?>">
                        </div>
                      </div>
                </div>
                </div> 
                <div class="card-header">
                <h3 class="card-title"></h3>
                <button type="submit"  class="btn btn-default float-right" id="btnActualizar" name="btnActualizar"><i class="fas fa-pencil-alt"> Editar</i> </button>
              </div>  
              </div>
              <!-- /.card-body -->

            </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    </form>
    <?php   

      //Llamar al metodo update para guardar /modificar datos

      if (isset($_POST["btnActualizar"])) {

        $vrTRid=$id;

        $vrTRnombre  = htmlspecialchars($_POST["TRnombre"]);
        $vrTRcaracteristicas  = htmlspecialchars($_POST["TRcaracteristicas"]);
        $vrTRpuntos  = htmlspecialchars($_POST["TRpuntos"]);

        $objCrud = new Crud();
        $objCrud ->tablas     = "tipo_reciclaje";
        $objCrud ->expresion  = " nombre = '$vrTRnombre', caracteristicas = '$vrTRcaracteristicas', puntos_botella = '$vrTRpuntos'";
        //$objCrud ->expresion ="'Doc_id'";
        
        $objCrud ->condicion = "id_tipo_reciclaje='$vrTRid'";
        $objCrud ->update();
        $_SESSION['message'] = 'Tipo de reciclaje actualizado!!!';
        $_SESSION['message_type'] = 'primary';
        //include ("alluser.php");
        echo "<a href='allTipoR.php' >Verificar</a>";



      }


    ?>
    <!-- /.content -->
  </div>
    <?php 
          
          } 
        ?> 

   <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>
